import json
from fuzzywuzzy import process, fuzz
import numpy as np
from copy import deepcopy

# open json files
equipment = None
equip_names = []
exercises = None
def open_files():
    global equipment, exercises, equip_names

    with open('equipment.json', encoding='utf8') as f:
        equipment = json.load(f)
        equip_names = [e['name'] for e in equipment]
    with open('exercises.json', encoding='utf8') as f:
        exercises = json.load(f)

# globals
pairs = []             # [exercise name, its equipment]
unmatched_exers = []   # [{name, equipment, attempted-match, score}]
matched_exers = []     # full exercise instances
# equipment:
# null             map to "Bodyweight" (mostly stretches)
# body only        map to "Bodyweight"
# machine          match exercise name to equipment name
# other            match exercise name to equipment name
# foam roll        map to "Foam Roller"
# kettlebells      map to "Kettlebells"
# dumbbell         map to "Dumbbells - Fixed and Adjustable"
# cable            match heuristic to cable crossover machine or functional trainer
# barbell          map to "Barbells and Olympic Barbells"
# bands            map to "Resistance Bands"
# medicine ball    map to "Medicine Ball"
# exercise ball    map to "Exercise Ball / Stability Ball"
# e-z curl bar     map to "E-Z Curl Bar"
map_key = {
    'null': 'Bodyweight',
    'body only': 'Bodyweight',
    'foam roll': 'Foam Roller',
    'kettlebells': 'Kettlebells',
    'dumbbell': 'Dumbbells - Fixed and Adjustable',
    'barbell': 'Barbells and Olympic Barbells',
    'bands': 'Resistance Bands',
    'medicine ball': 'Medicine Ball',
    'exercise ball': 'Exercise Ball / Stability Ball',
    'e-z curl bar': 'E-Z Curl Bar'
}

# Changes the given exercise's equipment field to 
# reflect the value it should be mapped to
def map_generic(exercise):
    global map_key
    key = exercise['equipment'] if exercise['equipment'] else 'null'
    
    # disregard custom equipment
    if key in map_key.keys():
        # change field value
        exercise['equipment'] = map_key[key]

    # add to pairs
    pairs.append((exercise['name'], exercise['equipment']))
    matched_exers.append(exercise)

# Match exercise name to equipment name
def match_generic(exercise):
    global equip_names, unmatched_exers
    
    eq_match = None
    score = -1
    # extract bests
    # of bests, extractOne with partial_ratio
    # maybe muscle matching in the future...
    # change field value
    exer_name = exercise['name']
    bests = process.extractBests(exer_name, equip_names, score_cutoff=70, limit=10)
    # bests returns list of tuples (eq_match, score)
    match len(bests):
        case 0:
            eq_match = None
        case 1:
            eq_match = bests[0][0]
        case _:
            choices = (t[0] for t in bests)
            eq_match, score = process.extractOne(exer_name, choices, scorer=fuzz.partial_ratio)
    
    return (eq_match, score)

# See match_generic()
def match_machine(exercise):
    global pairs, unmatched_exers

    eq_match, score = match_generic(exercise)
    if eq_match:
        exercise['equipment'] = eq_match
        pairs.append((exercise['name'], eq_match))
        matched_exers.append(exercise)
    else:
        unmatched_exers.append({
            "name": exercise['name'],
            "equipment": exercise['equipment'],
            "attempted-match": eq_match,
            "score": score
        })

# Match to one of two cable machines
# "Cable Crossover Machine" or "Functional Trainer"
def match_cable(exercise):
    global pairs

    # Since FT encompasses all muscles we only check CCM muscles
    ccm_muscles = {'chest', 'shoulders', 'back', 'lower back', 'middle back'}
    exer_muscles = {*exercise['primaryMuscles'], *exercise['secondaryMuscles']}
    eq_match = match_generic(exercise)[0]
    
    # if match null or cable in name
    if 'Cable' in exercise['name'] or 'High Pulley' in exercise['name'] or not eq_match:
        if exer_muscles.issubset(ccm_muscles):
            exercise['equipment'] = 'Cable Crossover Machine'
        else:
            exercise['equipment'] = 'Functional Trainer'
    else:
        exercise['equipment'] = eq_match
    
    # print(f'{exercise["name"]} , Muscles: {exer_muscles}')
    # print(f'match: {exercise["equipment"]}')
    # print(f'match: {match if match else exercise["equipment"]}')
    # print()

    # add to pairs
    pairs.append((exercise['name'], exercise['equipment']))
    matched_exers.append(exercise)



def match_other(exercise): 
    global pairs, unmatched_exers

    # special case where category is plyometrics
    if exercise['category'] == 'plyometrics':
        exercise['equipment'] = 'Plyometric Box / Jump Box'
        # add to pairs
        pairs.append((exercise['name'], exercise['equipment']))
        matched_exers.append(exercise)
    # match exercise name to equipment name
    else:
        eq_match, score = match_generic(exercise)
        if eq_match:
            exercise['equipment'] = eq_match
            pairs.append((exercise['name'], eq_match))
            matched_exers.append(exercise)
        else:
            unmatched_exers.append({
                "name": exercise['name'],
                "equipment": exercise['equipment'],
                "attempted-match": eq_match,
                "score": score
            })


def match_everything():
    global exercises
    for exer in exercises:
        # Map if not Special Case
        match exer['equipment']:
            case 'machine':
                match_machine(exer)
            case 'other':
                match_other(exer)
            case 'cable':
                match_cable(exer)
            case _:
                map_generic(exer)
    
#     # null / body only
#     if not exer['equipment'] or exer['equipment'] == 'body only':
#         pairs.append((exer['name'], 'bodyweight'))
#         continue

#     # String to match equipment to
#     match = exer['equipment'] if exer['equipment']!='machine' \
#     and exer['equipment']!='other' else exer['name']
    
#     equip, score = process.extractOne(match, equip_names)
    
#     if score > 80:
#         pairs.append((exer['name'], equip))
#     else:
#         unmatched_exers.append({
#             "name":exer['name'],
#             "equipment":exer['equipment'],
#             "attempted-match":equip,
#             "score":score
#         })

# Extract x amount of equipment and return a list
def extract_equipment(x):
    global matched_exers, equip_names

    print(f'match count: {len(matched_exers)}')
    print(f'ename count: {len(equip_names)}')
    enames = deepcopy(equip_names)
    e = []

    # mandatory: populate e with all equipment types
    while len(enames) != 0:
        for exer in matched_exers:
            ename = exer['equipment']
            if ename in enames:
                enames.remove(ename)
                e.append(exer)
                matched_exers.remove(exer)
    
    # add random exercises until size quota filled
    erandom = np.random.choice(matched_exers, size=(x-100))
    
    # join the lists
    e.extend(erandom)

    return e

# Writes to output files after matching has been processed
def write_files ():
    global pairs, matched_equip, equip_names

    # write out pairs
    with open('pairs.txt', 'w') as f:
        json.dump(pairs, f, indent=4)

    # write out unmatched equipment
    with open('unmatched.txt', 'w') as f:
        # find unmatched equipment
        unmatched_equip = []
        matched_equip = [p[1] for p in pairs]
        for name in equip_names:
            if name not in matched_equip:
                unmatched_equip.append(name)
        json.dump(unmatched_equip, f, indent=4)

        f.write('\n\n\n##########\n\n\n')

        json.dump(unmatched_exers, f, indent=4)
    
    # writes out exercises with equipment matches
    with open('extracted_equipment.json', 'w') as f:
        json.dump(extract_equipment(150), f, indent=4)


# exer_names = [e['name'] for e in exercises if e['name']]
# exer_equips = [(e['name'], e['equipment']) for e in exercises if e['equipment'] == 'other']

# print(exer_equips)

# map = []
# for equip in equipment:
    


# with open('out.txt', 'w') as f:
#     for e in exercises:
#         if e['equipment']:
#             f.write(e['equipment'] + '\n')

# Extract 150 Exercises into json file
# Make sure each equipment appears at least once

if __name__ == '__main__': # pragma: no cover
    open_files()
    match_everything()
    write_files()