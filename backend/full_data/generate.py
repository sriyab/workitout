import json
import random
from tokenize import String

all_exercises = None
with open('../data/exercises.json') as f:
    all_exercises = json.load(f)
all_exercises = {e['name'] : e for e in all_exercises}

all_equipment = None
with open('../data/equipment_youtube.json') as f:
    all_equipment = json.load(f)
all_equipment = {e['name'] : e for e in all_equipment}


data = []

try: 
    for i in range(10):
        print("\033[H\033[2J", end="")
        print("instance: ", i)

        instance = {
            'name': "",
            'author': "WorkItOut Staff",
            'link': "",
            'featured': False,
            'difficulty': -1,
            'length': -1,
            'type': "",
            'equipment': [],
            'exercises': [],
            'overview': "",
            'media': None
        }

        ### Decide on what exercises to use
        usr_exers = []
        while True:
            choices = random.sample([*all_exercises.keys()], 10)
            print(usr_exers)
            for choice in enumerate(choices):
                print(*choice)
            
            usr = input("Choices (or stop):\n")
            if usr == "":
                continue
            if usr == "stop":
                break

            for num in usr.split(","):
                usr_exers.append(choices[int(num)])
            
            print("\033[H\033[2J", end="")
        instance['exercises'] = usr_exers

        ### Create equipment from exercises
        equipment = set()
        for exercise in instance['exercises']:
            e = all_exercises[exercise]['equipment']
            equipment.add(e)
            assert e in all_equipment.keys()
        instance['equipment'] = list(equipment)

        ### create the timings for each 
        timings = []
        for exer in usr_exers:
            print("\033[H\033[2J", end="")
            print("Exercises: ", usr_exers)
            print()
            print(exer + ": ")
            s = input("sets: ")
            r = input("reps (s?): ")
            if 's' in r:
                # sets * seconds
                timings.append({"exercise": exer, "time": int(s) * int(r.removesuffix('s')), "sets":s, "reps":r.removesuffix('s'), "sec": True})

            else:
                timings.append({"exercise": exer, "time": int(s)*3, "sets":s, "reps":r, "sec" : False})

        ### determine length of routine
        length = 0
        for e in timings:
            length += int(round(e['time'] / 60))
        instance['length'] = length

        ### name
        print("\033[H\033[2J", end="")
        print(usr_exers)
        instance['name'] = input("Routine name: ")

        ### Routine difficulty
        print("\033[H\033[2J", end="")
        print(usr_exers)
        instance['difficulty'] = input("Routine difficulty (0, 1, 2): ")

        ### Routine type
        print("\033[H\033[2J", end="")
        print(usr_exers)
        usr = input("Cardio, Mobility, or Strength? (c, m, s): ")
        if usr == 'c': instance['type'] = "Cardio"
        elif usr == 'm': instance['type'] = "Mobility"
        elif usr == 's': instance['type'] = "Strength"
        else:
            input("ur a failure")
        

        ### Overview
        overview = ""
        for e in timings:
            if e['sec']:
                overview += f"{e['exercise']} for {e['sets']}*{e['reps']} seconds\n"
            else:
                overview += f"{e['exercise']} for {e['sets']}*{e['reps']} reps\n"
        instance['overview'] = overview

        print(instance)
        input()

        ### fill media
        equip_imgs = []
        equip_vids = []
        exer_imgs = []
        exer_vids = []
        for equip in instance['equipment']:
            equip_imgs.append(all_equipment[equip]['media']['image'])
            equip_vids.append(all_equipment[equip]['media']['video'])
        for exer in instance['exercises']:
            exer_imgs.append(all_exercises[exer]['media']['image'])
            exer_vids.append(all_exercises[exer]['media']['video'])
        instance['media'] = {
            "image": equip_imgs + exer_imgs,
            "video": equip_vids + exer_vids,
            "equipment_images": equip_imgs,
            "equipment_videos": equip_vids,
            "exercise_images": exer_imgs,
            "exercise_videos": exer_vids
        }

        data.append(instance)
except:
    # don't lose data
    with open('failed1.json', 'w') as f:
        json.dump(data, f, indent=4)
        exit()

with open('11-5-out-1.json', 'w') as f:
        json.dump(data, f, indent=4)


        


        

    



