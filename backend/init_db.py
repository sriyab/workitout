import json
from models import db, app, Equipment, Exercises, Routines, Muscles

# JSON file names
JSON_EQUIPMENT = 'data/equipment_youtube.json'
JSON_EXERCISES = 'data/exercises.json'
JSON_ROUTINES = 'data/routines.json'

# common keys
NAME_KEY = 'name'
CATEGORY_KEY = 'category'
MEDIA_KEY = 'media'
IMAGE_KEY = 'image'
VIDEO_KEY = 'video'

# specific keys
EQ_MUSCLE_KEY = 'muscles-used'
EQ_SUITABILITY_KEY = 'suitability'
EQ_PRICE_KEY = 'price'
EQ_DESCRIPTION_KEY = 'description'
EQ_TIPS_KEY = 'tips'
EX_FORCE_KEY = 'force'
EX_LEVEL_KEY = 'level'
EX_MECHANIC_KEY = 'mechanic'
EX_EQUIPMENT_KEY = 'equipment'
EX_MUSCLES_PRIMARY_KEY = 'primaryMuscles'
EX_MUSCLES_SECONDARY_KEY = 'secondaryMuscles'
EX_INSTRUCTIONS_KEY = 'instructions'
RT_AUTHOR_KEY = 'author'
RT_LINK_KEY = 'link'
RT_DIFFICULTY_KEY = 'difficulty'
RT_LENGTH_KEY = 'length'
RT_TYPE_KEY = 'type'
RT_EQUIPMENT_KEY = 'equipment'
RT_EXERCISES_KEY = 'exercises'
RT_OVERVIEW_KEY = 'overview'


# NOTE: Current structure based on Canyon's implementation
# https://gitlab.com/noy-nac/cs373-idb/-/blob/main/backend/db.py

# Make integer IDs unique across db
id_global = 1

# Reset database
def reset_db():
    print('resetting db')
    db.session.remove()
    db.drop_all()
    db.create_all()

# Populate database
def populate_db():
    print('populating db')
    
    populate_equipment()
    populate_exercises()
    populate_routines()
    

# Populate database with equipment
def populate_equipment():
    global id_global

    json_file = JSON_EQUIPMENT
    with open(json_file) as jsn:
        equipment_data = json.load(jsn)
        
        for equipment in equipment_data:
            record = dict({
                'id': id_global,
                'name': equipment[NAME_KEY],
                'category': equipment[CATEGORY_KEY],
                'muscles': [], # filled later,
                'muscles_string': equipment[EQ_MUSCLE_KEY],
                'home_suitable': equipment[EQ_SUITABILITY_KEY][0],
                'commercial_suitable': equipment[EQ_SUITABILITY_KEY][1],
                'price_low': equipment[EQ_PRICE_KEY][0],
                'price_high': equipment[EQ_PRICE_KEY][1],
                'description': equipment[EQ_DESCRIPTION_KEY],
                'tips': equipment[EQ_TIPS_KEY],
                'images': equipment[MEDIA_KEY][IMAGE_KEY],
                'videos': equipment[MEDIA_KEY][VIDEO_KEY]
            }) 

            # association to muscles
            muscle_list = equipment[EQ_MUSCLE_KEY]
            add_muscles(record, 'muscles', muscle_list)
            # for muscle in muscle_list:
            #     muscle_query = db.session.query(Muscles).filter(
            #         Muscles.id == muscle).all()

            #     assert len(muscle_query) <= 1

            #     if len(muscle_query) == 1: 
            #         # muscle in table
            #         muscle_obj = muscle_query[0]
            #         record['muscles'].append(muscle_obj)
            #     else: 
            #         # muscle not in table, create new entry
            #         muscle_obj = Muscles(id=muscle)
            #         record['muscles'].append(muscle_obj)

            # add record
            r = Equipment(**record)
            db.session.add(r)

            # increment id 
            id_global += 1
        
        # commit session
        db.session.commit()

def populate_exercises():
    global id_global

    json_file = JSON_EXERCISES
    with open(json_file) as jsn:
        exercise_data = json.load(jsn)
        for exercise in exercise_data:
            # find matching equipment
            equipment = db.session.query(Equipment).filter(
                Equipment.name == exercise[EX_EQUIPMENT_KEY]
            ).first()

            record = dict({
                'id': id_global,
                'name': exercise[NAME_KEY],
                'category': exercise[CATEGORY_KEY],
                'force': exercise[EX_FORCE_KEY],
                'level': exercise[EX_LEVEL_KEY],
                'mechanic': exercise[EX_MECHANIC_KEY],
                'equipment_id': equipment.id,
                'equipment_name': exercise[EX_EQUIPMENT_KEY],
                'muscles_primary': [], # filled later
                'muscles_primary_string': exercise[EX_MUSCLES_PRIMARY_KEY],
                'muscles_secondary': [], # filled later
                'muscles_secondary_string': exercise[EX_MUSCLES_SECONDARY_KEY],
                'instructions': exercise[EX_INSTRUCTIONS_KEY],
                'images': exercise[MEDIA_KEY][IMAGE_KEY],
                'videos': exercise[MEDIA_KEY][VIDEO_KEY]
            })

            # association to muscles
            muscle_list = exercise[EX_MUSCLES_PRIMARY_KEY]
            add_muscles(record, 'muscles_primary', muscle_list)
            muscle_list = exercise[EX_MUSCLES_SECONDARY_KEY]
            add_muscles(record, 'muscles_secondary', muscle_list)

            # association to equipment
            # similar logic to association to muscles
            # % match equipment names with fuzzywuzzy
            # for now, check if equipment in table otherwise add to file
            # flag new equipment to add to equipment.json

            # add record
            r = Exercises(**record)
            db.session.add(r)

            # increment id
            id_global += 1

        # commit session
        db.session.commit()

# adds muscles to record
def add_muscles(record, key, muscle_list):
    for muscle in muscle_list:
        muscle_query = db.session.query(Muscles).filter(
            Muscles.id == muscle).all()
        
        assert len(muscle_query) <= 1

        if len(muscle_query):
            # muscle in table
            muscle_obj = muscle_query[0]
            record[key].append(muscle_obj)
        else:
            # muscle not in table, create new entry
            muscle_obj = Muscles(id=muscle)
            record[key].append(muscle_obj)



def populate_routines():
    global id_global

    json_file = JSON_ROUTINES
    with open(json_file, 'r', encoding='utf-8') as jsn:
        routine_data = json.load(jsn)
        
        for routine in routine_data:
            record = dict({
                'id': id_global,
                'name': routine[NAME_KEY],
                'author': routine[RT_AUTHOR_KEY],
                'link': routine[RT_LINK_KEY],
                #'featured': routine['featured'],
                'difficulty': routine[RT_DIFFICULTY_KEY],
                'length': routine[RT_LENGTH_KEY],
                'type': routine[RT_TYPE_KEY],
                'equipment': [], # filled later
                'exercises': [], # filler later
                'overview': routine[RT_OVERVIEW_KEY]
                # 'images': routine[MEDIA_KEY][IMAGE_KEY],
                # 'videos': routine[MEDIA_KEY][VIDEO_KEY]
            }) # fill 

            # association to exercises
            exercise_list = routine[RT_EXERCISES_KEY]
            for exercise in exercise_list:
                exercise_query = db.session.query(Exercises).filter(
                    Exercises.name == exercise).all()
                assert len(exercise_query) <= 1 # valid exercises
                if len(exercise_query):
                    exercise_obj = exercise_query[0]
                    record['exercises'].append(exercise_obj)
                else:
                    print(f'exercise not added: {exercise}')

            # association to equipment
            equipment_list = routine[RT_EQUIPMENT_KEY]
            for equipment in equipment_list:
                equipment_query = db.session.query(Equipment).filter(
                    Equipment.name == equipment).all()
                assert len(equipment_query) <= 1 # valid equipment
                if len(equipment_query):
                    equipment_obj = equipment_query[0]
                    record['equipment'].append(equipment_obj)
                else:
                    print(f'exercise not added: {equipment}')

            # add and commit
            r = Routines(**record)
            db.session.add(r)

            id_global += 1
        
        db.session.commit()

with app.app_context():
    if __name__ == '__main__': # pragma: no cover
        reset_db()
        populate_db()
