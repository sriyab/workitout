import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# Login variables
USER = 'postgres'
PASS = 'workit0ut123!'
RDS_ENDPOINT = 'workit0ut.cdys7ix6ukzn.us-east-2.rds.amazonaws.com'
DB_NAME = 'workit0ut_db'

# Connect to DB
app = Flask(__name__)
url = os.environ.get(
    "DB_STRING", f'postgresql://{USER}:{PASS}@{RDS_ENDPOINT}/{DB_NAME}'
)
app.config['SQLALCHEMY_DATABASE_URI'] = url

cors = CORS(app)
db = SQLAlchemy(app)



# Link Tables
# model links
# routine / equipment
routine_equipment_link = db.Table('routine_equipment_link',
                                db.Column('routine_id', db.Integer,
                                          db.ForeignKey('routines.id')),
                                db.Column('equipment_id', db.Integer,
                                          db.ForeignKey('equipment.id')))
routine_exercise_link = db.Table('routine_exercise_link',
                                db.Column('routine_id', db.Integer,
                                          db.ForeignKey('routines.id')),
                                db.Column('exercise_id', db.Integer,
                                          db.ForeignKey('exercises.id')))
# routine / exercise
# exercise / equipment many to one, association table not needed
# muscle links
equipment_muscle_link = db.Table('equipment_muscle_link',
                                db.Column('equipment_id', db.Integer,
                                          db.ForeignKey('equipment.id')),
                                db.Column('muscle_id', db.String(),
                                          db.ForeignKey('muscles.id')))
exercise_muscle_primary_link = db.Table('exercise_muscle_primary_link',
                                db.Column('exercise_id', db.Integer,
                                          db.ForeignKey('exercises.id')),
                                db.Column('muscle_id', db.String(),
                                          db.ForeignKey('muscles.id')))
exercise_muscle_secondary_link = db.Table('exercise_muscle_secondary_link',
                                db.Column('exercise_id', db.Integer,
                                          db.ForeignKey('exercises.id')),
                                db.Column('muscle_id', db.String(),
                                          db.ForeignKey('muscles.id')))
                                

# Models
class Equipment(db.Model):
    __tablename__ = 'equipment'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    category = db.Column(db.String())
    muscles = db.relationship(
        'Muscles', secondary='equipment_muscle_link')
    muscles_string = db.Column(db.String())
    home_suitable = db.Column(db.Boolean)
    commercial_suitable = db.Column(db.Boolean)
    price_low = db.Column(db.Integer)
    price_high = db.Column(db.Integer)
    description = db.Column(db.Text)
    tips = db.Column(db.Text)
    images = db.Column(db.ARRAY(db.String))
    videos = db.Column(db.ARRAY(db.String))
    exercises = db.relationship('Exercises', backref='equipment')

    # Ref: https://stackoverflow.com/questions/5022066/how-to-serialize-sqlalchemy-result-to-json
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


# class Muscles_Eq(db.Model):
#     __tablename__ = 'muscles_eq'

#     id = db.Column(db.Integer, primary_key=True)
#     equipment = db.Column(db.Integer, db.ForeignKey('equipment.id'))
#     muscle = db.Column(db.String())

#     def as_dict(self):
#         return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Exercises(db.Model):
    __tablename__ = 'exercises'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    category = db.Column(db.String())
    force = db.Column(db.String())
    level = db.Column(db.String())
    mechanic = db.Column(db.String())
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.id'))
    equipment_name = db.Column(db.String())
    muscles_primary = db.relationship(
        'Muscles', secondary='exercise_muscle_primary_link')
    muscles_primary_string = db.Column(db.String())
    muscles_secondary = db.relationship(
        'Muscles', secondary='exercise_muscle_secondary_link')
    muscles_secondary_string = db.Column(db.String())
    # ^ primary/secondary distinguised in init_db populate
    instructions = db.Column(db.Text)
    images = db.Column(db.ARRAY(db.String))
    videos = db.Column(db.ARRAY(db.String))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Routines(db.Model):
    __tablename__ = 'routines'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    author = db.Column(db.String())
    link = db.Column(db.String())
    type = db.Column(db.String())
    equipment = db.relationship(
        'Equipment', secondary='routine_equipment_link', backref='routines_eq')
    exercises = db.relationship(
        'Exercises', secondary='routine_exercise_link', backref='routines_ex')
    difficulty = db.Column(db.Integer())
    length = db.Column(db.Integer())
    overview = db.Column(db.String())
    # images = db.Column(db.ARRAY(db.String))
    # videos = db.Column(db.ARRAY(db.String))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

# Generic
class Muscles(db.Model):
    __tablename__ = 'muscles'

    id = db.Column(db.String(), primary_key=True)

if __name__ == '__main__': # pragma: no cover
    print(db)