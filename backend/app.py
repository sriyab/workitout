from cgitb import reset
from flask import Flask, jsonify, request, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, and_, func
from init_db import db, app, Equipment, Exercises, Routines, reset_db, populate_db
from models import routine_equipment_link, routine_exercise_link
from schemas import equipment_schema, exercise_schema, routine_schema

import os

@app.before_first_request
def create_tables():
    # reset_db()
    # populate_db()
    print('COMMENT')

@app.route('/')
def index():
    return "Backend is running!"

# API format/design adapted from Bookrus Spring 2022 project
@app.route('/exercises', methods=['GET'])
def get_exercises():
    query = db.session.query(Exercises)

    # Pagination
    page = request.args.get("page", type=int)
    limit = request.args.get("limit", type=int)
    # Sorting (Name)
    sort = request.args.get("sort")
    # Searching
    search = request.args.get("search")
    # Filtering
    force = request.args.get("force")
    level = request.args.get("level")
    mechanic = request.args.get("mechanic")
    category = request.args.get("category")

    # Filtering the results
    if force is not None:
        # push, pull, static, or null
        # force = force.replace(" ", "&")
        query = query.filter(Exercises.force.ilike(force))
    if level is not None:
        # Might want to change to include multiple levels
        # level = level.replace(" ", "&")
        query = query.filter(Exercises.level.ilike(level))
    if mechanic is not None:
        # There is numerous mechanics, so might want to filter by multiple
        mechanic = mechanic.replace(" and ", "&")
        query = query.filter(Exercises.mechanic.ilike(mechanic))
    if category is not None:
        # Numerous categories, so filter by multiple
        category = category.replace("and", "&")
        query = query.filter(Exercises.category.ilike(category))

    # TODO: Figure out how to add regex tokens
    if search is not None:
        search = search.split(" ")
        results = []
        for key in search:
            results.append(Exercises.name.ilike("%" + key + "%"))
            results.append(Exercises.category.ilike("%" + key + "%"))
            results.append(Exercises.muscles_primary_string.ilike("%" + key + "%"))
            results.append(Exercises.muscles_secondary_string.ilike("%" + key + "%"))
            results.append(Exercises.force.ilike("%" + key + "%"))
            results.append(Exercises.mechanic.ilike("%" + key + "%"))
            results.append(Exercises.equipment_name.ilike("%" + key + "%"))
            results.append(Exercises.instructions.ilike("%" + key + "%"))
        # put try except clauses for int searches
        query = query.filter(or_(*results))

    # Sort the results
    # TODO: Add "assertion" to ensure attribute is sortable
    if sort is not None:
        # sort = sort.replace("-", "_")
        if getattr(Exercises, sort, None) is not None:
            query = query.order_by(getattr(Exercises, sort))

    count = query.count()

    # Getting all instances of exercises
    if page is not None:
        query = query.paginate(page=page, per_page=limit, error_out=False).items
    exercises = exercise_schema.dump(query, many=True)
    return jsonify(
        {
            "Exercises": exercises,
            "meta_total": count
        }
    )

@app.route('/exercises/<id>', methods=['GET'])
def get_exercise_id(id):
    query = db.session.query(Exercises).filter_by(id=id)
    exercise = exercise_schema.dump(query, many=True)[0]
    if not exercise:
        return Response('invalid id', status=404)
    return jsonify(exercise)

@app.route('/equipment', methods=['GET'])
def get_equipment():
    query = db.session.query(Equipment)

    # Pagination
    page = request.args.get("page", type=int)
    limit = request.args.get("limit", type=int)
    # Sorting (Name, Price (high and low))
    sort = request.args.get("sort")
    # Searching
    search = request.args.get("search")
    # Filtering
    category = request.args.get("category")
    muscles = request.args.get("muscles")
    home_suitable = request.args.get("home_suitable")
    commercial_suitable = request.args.get("commercial_suitable")

    # Filtering the results
    if category is not None:
        query = query.filter(Equipment.category.ilike(category))
    # TODO: Fix this filter because muscles is a list of muscles, not a variable
    # TODO: See if muscles_string solves filter problem
    if muscles is not None:
        # query = query.filter(Equipment.muscles_string.any().ilike(muscles))
        query = query.filter(Equipment.muscles_string.ilike("%" + muscles + "%"))
    if home_suitable is not None:
        # Value passed in is either "true" or "false"
        boolean = True if home_suitable == "true" else False
        query = query.filter(Equipment.home_suitable == boolean)
    if commercial_suitable is not None:
        # Value passed in is either "true" or "false"
        boolean = True if commercial_suitable == "true" else False
        query = query.filter(Equipment.commercial_suitable == boolean)

    if search is not None:
        search = search.split(" ")
        results = []
        for key in search:
            results.append(Equipment.name.ilike("%" + key + "%"))
            results.append(Equipment.muscles_string.ilike("%" + key + "%"))
            results.append(Equipment.category.ilike("%" + key + "%"))
            results.append(Equipment.description.ilike("%" + key + "%"))
            results.append(Equipment.tips.ilike("%" + key + "%"))
        # put try except clauses for int searches
        query = query.filter(or_(*results))

    # TODO: Add "assertion" to ensure attribute is sortable
    if sort is not None:
        # sort = sort.replace("-", "_")
        if getattr(Equipment, sort, None) is not None:
            query = query.order_by(getattr(Equipment, sort))

    count = query.count()

    # Getting all instances of equipment
    if page is not None:
        query = query.paginate(page=page, per_page=limit, error_out=False).items
    equipment = equipment_schema.dump(query, many=True)
    return jsonify(
        {
            "Equipment": equipment,
            "meta_total": count
        }
    )

@app.route('/equipment/<id>', methods=['GET'])
def get_equipment_id(id):
    query = db.session.query(Equipment).filter_by(id=id)
    equipment = equipment_schema.dump(query, many=True)[0]
    if not equipment:
        return Response('invalid id', status=404)
    return jsonify(equipment)

#### sample sql for filtering equipment in the future:
# select 
# 	eq.name,
# 	mu.id
# from equipment_muscle_link em
# inner join Equipment eq
# on em.equipment_id = eq.id
# inner join muscles mu
# on em.muscle_id = mu.id
# where mu.id = 'back'


@app.route('/routines', methods=['GET'])
def get_routines():
    query = db.session.query(Routines)

    # Pagination
    page = request.args.get("page", type=int)
    limit = request.args.get("limit", type=int)
    # Sorting (Length, Author, Name)
    sort = request.args.get("sort")
    # Searching
    search = request.args.get("search")
    # Filtering
    difficulty = request.args.get("difficulty")
    type = request.args.get("type")

    # Filtering the results
    if difficulty is not None:
        query = query.filter(Routines.difficulty == int(difficulty))
    if type is not None:
        query = query.filter(Routines.type.ilike(type))
    
    if search is not None:
        search = search.split(" ")
        results = []
        for key in search:
            results.append(Routines.name.ilike("%" + key + "%"))
            results.append(Routines.author.ilike("%" + key + "%"))
            results.append(Routines.overview.ilike("%" + key + "%"))
            results.append(Routines.type.ilike("%" + key + "%"))
        # put try except clauses for int searches
        # TODO: Might need to add clauses for exercises, equipment, and length
        query = query.filter(or_(*results))

    # TODO: Add "assertion" to ensure attribute is sortable
    if sort is not None:
        # sort = sort.replace("-", "_")
        if getattr(Routines, sort, None) is not None:
            query = query.order_by(getattr(Routines, sort))

    count = query.count()

    # Getting all instances of equipment
    if page is not None:
        query = query.paginate(page=page, per_page=limit, error_out=False).items
    routines = routine_schema.dump(query, many=True)
    return jsonify(
        {
            "Routines": routines,
            "meta_total": count
        }
    )

@app.route('/routines/<id>', methods=['GET'])
def get_routine_id(id):
    query = db.session.query(Routines).filter_by(id=id)
    routine = routine_schema.dump(query, many=True)[0]
    if not routine:
        return Response('invalid id', status=404)
    return jsonify(routine)

# Related endpoints
@app.route('/exercises/<id>/related', methods=['GET'])
def get_exercise_related(id):
    query = db.session.query(Exercises).filter_by(id=id)
    exercise = exercise_schema.dump(query, many=True)[0]
    if not exercise:
        return Response('invalid id', status=404)

    related = {
        'equipment': [],
        'routines': []
    }

    # get related equipment
    related['equipment'].append(exercise['equipment_id'])

    # get related routines
    # query the association table, 
    # loop thru each related instance and return its id
    exercise = db.session.query(Exercises).filter_by(id=id).first()
    print(exercise.routines_ex)
    for r in exercise.routines_ex:
        related['routines'].append(r.id)

    return jsonify(related)

@app.route('/equipment/<id>/related', methods=['GET'])
def get_equipment_related(id):
    query = db.session.query(Equipment).filter_by(id=id)
    equipment = equipment_schema.dump(query, many=True)[0]
    if not equipment:
        return Response('invalid id', status=404)
        
    related = {
        'exercises': [],
        'routines': []
    }

    # get related exercises
    equipment = db.session.query(Equipment).filter_by(id=id).first()
    for e in equipment.exercises:
        related['exercises'].append(e.id)

    # get related routines
    # query the association table, 
    # loop thru each related instance and return its id
    print(equipment.exercises)
    for r in equipment.routines_eq:
        related['routines'].append(r.id)

    return jsonify(related)
    
@app.route('/routines/<id>/related', methods=['GET'])
def get_routine_related(id):
    query = db.session.query(Routines).filter_by(id=id)
    routine = routine_schema.dump(query, many=True)[0]
    if not routine:
        return Response('invalid id', status=404)
        
    related = {
        'equipment': routine['equipment'],
        'exercises': routine['exercises']
    }
    return jsonify(related)        

if __name__ == "__main__":
    app.run(debug = True)
