Create environment and install dependencies   
```
python3 -m venv venv     
source venv/bin/activate   
pip3 install -r requirements.txt   
```    
EC2 Instance Flask instructions
```
source venv/bin/activate
cd workitout/backend
git pull
sudo systemctl daemon-reload
sudo systemctl restart workitout.service
sudo systemctl restart nginx
```