import app      # routes
import unittest # main, TestCase
import json     # loads
# from init_db import db, app, Equipment, Exercises, Routines

# -------------
# TestEndpoints
# -------------

class TestEndpoints(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()
    
    # ----
    # eval
    # ----

    # /
    def test_home(self):
        with self.client:
            response = self.client.get("/")
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)

    # /exercises
    def test_exercises_1(self):
        with self.client:
            response = self.client.get("/exercises")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json["Exercises"]), 150)

    def test_exercises_2(self):
        with self.client:
            response = self.client.get("/exercises?&page=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json["Exercises"]), 20)

    # /exercises/<id>
    def test_exercises_3(self):
        with self.client:
            response = self.client.get("/exercises/101")
            self.assertEqual(response.status_code, 200)

    # /exercises/<id>/related
    def test_exercises_4(self):
        with self.client:
            response = self.client.get("/exercises/101/related")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            num_related = len(res_json["equipment"]) + len(res_json["routines"])
            self.assertGreater(num_related, 0)

    # /exercises search
    def test_exercises_5(self):
        with self.client:
            response = self.client.get("/exercises?search=push")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreater(len(res_json["Exercises"]), 0);

    # /exercises filtering
    def test_exercises_6(self):
        with self.client:
            response = self.client.get("/exercises?search=push&category=cardio")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["Exercises"][0]["category"], "cardio")


    # /equipment
    def test_equipment_1(self):
        with self.client:
            response = self.client.get("/equipment")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json["Equipment"]), 100)

    def test_equipment_2(self):
        with self.client:
            response = self.client.get("/equipment?&page=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json["Equipment"]), 20)

    # /equipment/<id>
    def test_equipment_3(self):
        with self.client:
            response = self.client.get("/equipment/39")
            self.assertEqual(response.status_code, 200)

    # /equipment/<id>/related
    def test_equipment_4(self):
        with self.client:
            response = self.client.get("/equipment/39/related") 
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            num_related = len(res_json["exercises"]) + len(res_json["routines"])
            self.assertGreater(num_related, 0)

    # /equipment search
    def test_equipment_5(self):
        with self.client:
            response = self.client.get("/equipment?search=runni")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreater(len(res_json["Equipment"]), 0)

    # /equipment filter
    def test_equipment_6(self):
        with self.client:
            response = self.client.get("/equipment?category=free weights")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreater(len(res_json["Equipment"]), 0)
            self.assertEqual(res_json["Equipment"][0]["category"], "Free Weights")

    # /routines
    def test_routines_1(self):
        with self.client:
            response = self.client.get("/routines")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json["Routines"]), 50)

    def test_routines_2(self):
        with self.client:
            response = self.client.get("/routines?&page=1")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreaterEqual(len(res_json["Routines"]), 20)

    # /routines/<id>
    def test_routines_3(self):
        with self.client:
            response = self.client.get("/routines/275")
            self.assertEqual(response.status_code, 200)

    # /routines/<id>/related
    def test_routines_4(self):
        with self.client:
            response = self.client.get("/routines/275/related") 
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            num_related = len(res_json["exercises"]) + len(res_json["equipment"])
            self.assertGreater(num_related, 0)

    # /routines search
    def test_routines_5(self):
        with self.client:
            response = self.client.get("/routines?search=workitout")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreater(len(res_json["Routines"]), 0)

    # /routines filter
    def test_routines_6(self):
        with self.client:
            response = self.client.get("/routines?difficulty=2&search=work")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertGreater(len(res_json["Routines"]), 0)
            self.assertEqual(res_json["Routines"][0]["difficulty"], 2)

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
