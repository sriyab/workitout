from contextlib import nullcontext
import pip._vendor.requests as requests
from bs4 import BeautifulSoup
import lxml

def findYT(search):
    embed_text = "https://www.youtube.com/embed/"
    words = search.split()
    search_link = "https://www.youtube.com/results?search_query=" + '+'.join(words)

    search_result = requests.get(search_link).text
    remove_beginning = search_result.split("watch?v=",1)[1]
    first_search_result = remove_beginning.split("\"",1)[0]
    embed_text += first_search_result
    
    return embed_text

def findImage(search):
    words = search.split()
    search_words = '+'.join(words)
    url = 'https://www.google.com/search?q={0}&tbm=isch'.format(search_words)
    content = requests.get(url).content
    soup = BeautifulSoup(content,'lxml')
    images = soup.findAll('img')
    # print(images)

    return images[1].get('src')


if __name__ == '__main__':
    import json

    with open('exercises.json') as data_file:    
        data = json.load(data_file)
        print(type(data[0]))
        i = 0
        for v in data:
            print(i)
            if i < 150:
                video_link = findYT(v['name'])
                image_link = findImage(v['name'])
                v['media'] = { "image": [image_link], "video": [video_link] }

            i += 1

        out_file = open("exercises_media.json", "w")
    
        json.dump(data[:150], out_file, indent=2)
        
        out_file.close()
        
        # print(data)
