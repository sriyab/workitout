from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from models import Exercises, Equipment, Routines

# Adapted from bookrus project Spring 2022
class ExerciseSchema(SQLAlchemySchema):
    class Meta:
        model = Exercises
    
    id = auto_field()
    name = auto_field()
    category = auto_field()
    force = auto_field()
    level = auto_field()
    mechanic = auto_field()
    equipment_id = auto_field()
    equipment_name = auto_field()
    muscles_primary = auto_field()
    muscles_primary_string = auto_field()
    muscles_secondary = auto_field()
    muscles_secondary_string = auto_field()
    # ^ primary/secondary distinguised in init_db populate
    instructions = auto_field()
    images = auto_field()
    videos = auto_field()

exercise_schema = ExerciseSchema()

class EquipmentSchema(SQLAlchemySchema):
    class Meta:
        model = Equipment

    id = auto_field()
    name = auto_field()    
    category = auto_field()
    muscles = auto_field() 
    muscles_string = auto_field()   
    home_suitable = auto_field()
    commercial_suitable = auto_field()
    price_low = auto_field()
    price_high = auto_field()
    description = auto_field()
    tips = auto_field()
    images = auto_field()
    videos = auto_field()

equipment_schema = EquipmentSchema()

class RoutineSchema(SQLAlchemySchema):
    class Meta:
        model = Routines
    
    id = auto_field()
    name = auto_field()
    author = auto_field()
    link = auto_field()
    type = auto_field()
    equipment = auto_field()
    exercises = auto_field()
    difficulty = auto_field()
    length = auto_field()
    overview = auto_field()
    # images = auto_field()
    # videos = auto_field()

routine_schema = RoutineSchema()
