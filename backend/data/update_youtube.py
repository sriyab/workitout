from contextlib import nullcontext
import pip._vendor.requests as requests
from bs4 import BeautifulSoup
import lxml


def changeToEmbed(link):
    embed_text = "https://www.youtube.com/embed/"
    # print(link.split("https://www.youtube.com/watch?v=", 1))
    if "https://www.youtube.com/watch?v=" not in link:
        return link

    remove_beginning = link.split("https://www.youtube.com/watch?v=", 1)[1]
    embed_text += remove_beginning

    return embed_text


if __name__ == "__main__":
    import json

    with open("equipment.json") as data_file:
        data = json.load(data_file)
        print(type(data[0]))
        for v in data:
            print(v["media"]["video"])
            if v["media"]["video"] != []:
                video_link = changeToEmbed(v["media"]["video"][0])
                v["media"]["video"] = [video_link]

        out_file = open("equipment_youtube.json", "w")

        json.dump(data, out_file, indent=2)

        out_file.close()

        # print(data)
