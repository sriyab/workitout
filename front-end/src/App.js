import './App.css';
import Navigation from "./components/navbar"
import "bootstrap/dist/css/bootstrap.min.css"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import About from "./routes/about"
import Equipment from "./routes/equipment"
import Exercises from "./routes/exercises"
import Routines from "./routes/routines"
import Home from "./routes/home"
import Search from "./routes/search"
import Visualizations from './routes/visualizations';
import ProviderVisualizations from './routes/provider_visualizations';
import EquipmentPage from './components/equipment/equipment_page';
import ExercisePage from './components/exercises/exercise_page';
import RoutinePage from './components/routines/routine_page'

import React, { useEffect } from 'react';

function App() {
  useEffect(() => {
    document.title = "WorkItOut"
 }, []);

  return (
    <Router>
    <div>
      <header>
        <Navigation />
      </header>
       <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/exercises/:exerciseID" element={<ExercisePage />} />
        <Route path="/exercises" element={<Exercises />} />
        <Route path="/equipment/:equipmentID" element={<EquipmentPage />} />
        <Route path="/equipment" element={<Equipment />} />
        <Route path="/routines/:routineID" element={<RoutinePage />} />
        <Route path="/routines" element={<Routines />} />
        <Route path="/search" element={<Search />} />
        <Route path="/visualizations" element={<Visualizations />} />
        <Route path="/provider_visualizations" element={<ProviderVisualizations />} />
      </Routes>
    </div>
    </Router> 
  );
}

export default App;
