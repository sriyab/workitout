import React from "react"
import About from "../routes/about"
import Routines from "../routes/routines"
import Equipment from "../routes/equipment"
import Exercises from "../routes/exercises"
import EquipmentCard from "../components/equipment/equipment_card"
import RoutineCard from "../components/routines/routine_card"
import ExerciseCard from "../components/exercises/exercise_card"
import Home from "../routes/home"
import Search from "../routes/search"
import Navigation from "../components/navbar"
import { render, screen, cleanup } from '@testing-library/react'
import '@testing-library/jest-dom'
import { BrowserRouter } from "react-router-dom"

afterEach(cleanup);

describe("Navigation Bar", () => {
    test("Proper Rendering of Navigation Component", () => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<BrowserRouter><Navigation/></BrowserRouter>);
        
        // Make sure all of the navigation bars are included
        //
        expect(screen.getByText("Home")).toBeInTheDocument();
        expect(screen.getByText("Exercises")).toBeInTheDocument();
        expect(screen.getByText("Equipment")).toBeInTheDocument();
        expect(screen.getByText("Routines")).toBeInTheDocument();
        expect(screen.getByText("About")).toBeInTheDocument();

        console.error = originalError;

    });

});

describe("About Page", () => {
    test("Proper Rendering of About Page", () => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<About/>);

        expect(screen.getByText("Tools")).toBeInTheDocument();
        expect(screen.getByText("About our Site")).toBeInTheDocument();
        expect(screen.getByText("What's the Result?")).toBeInTheDocument();
        expect(screen.getByText("Members:")).toBeInTheDocument();

        console.error = originalError;

    });
});


describe("Home Page", () => {
    test(" Rendering Home Page", () => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<Home/>, {wrapper: BrowserRouter});
        
        expect(screen.getByText("Welcome to WorkItOut!")).toBeInTheDocument();
        expect(screen.getByText("Exercises")).toBeInTheDocument();
        expect(screen.getByText("Workout Routines")).toBeInTheDocument();
        expect(screen.getByText("Equipment")).toBeInTheDocument();

        console.error = originalError;
    });

});

describe("Testing Model Cards", () => {
    test("Proper Rendering of Equipment Card", ()=> {
        const originalError = console.error;
        console.error = jest.fn();
        render(<EquipmentCard/>, {wrapper: BrowserRouter});

        expect(screen.getByText("Category:")).toBeInTheDocument();
        expect(screen.getByText("Muscles Used:")).toBeInTheDocument();
        expect(screen.getByText("Price:")).toBeInTheDocument();
        expect(screen.getByText("Suitability:")).toBeInTheDocument();
        expect(screen.getByRole("button", {name: "See More"})).toBeInTheDocument();

        console.error = originalError;
    });

    test("Proper Rendering of Exercise Card", ()=> {
        const originalError = console.error;
        console.error = jest.fn();
        render(<ExerciseCard/>, {wrapper: BrowserRouter});

        expect(screen.getByText("Category:")).toBeInTheDocument();
        expect(screen.getByText("Level:")).toBeInTheDocument();
        expect(screen.getByText("Force:")).toBeInTheDocument();
        expect(screen.getByText("Primary Muscles:")).toBeInTheDocument();
        expect(screen.getByText("Equipment:")).toBeInTheDocument();
        expect(screen.getByRole("button", {name: "See More"})).toBeInTheDocument();

        console.error = originalError;
    });

    test("Proper Rendering of Routine Card", ()=> {
        const originalError = console.error;
        console.error = jest.fn();
        render(<RoutineCard/>, {wrapper: BrowserRouter});

        expect(screen.getByText("Author:")).toBeInTheDocument();
        expect(screen.getByText("Difficulty:")).toBeInTheDocument();
        expect(screen.getByText("Length:")).toBeInTheDocument();
        expect(screen.getByText("Type:")).toBeInTheDocument();
        expect(screen.getByRole("button", {name: "See More"})).toBeInTheDocument();

        console.error = originalError;
    });
});


describe("Test Exercises Page", () => {
    test("Proper Rendering of Exercises Page", async() => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<Exercises/>, {wrapper: BrowserRouter})
        
        // Make sure all the parts of the Splash page is included
        expect(screen.getByText("Exercises")).toBeInTheDocument();
        expect(screen.getByText("A list of all the exercises on Work It Out")).toBeInTheDocument();
        expect(screen.getByText("Desk Biking")).toBeInTheDocument();

        console.error = originalError;
    });

});


describe("Test Routines Page", () => {
    test("Proper Rendering of Routines Page", async() => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<Routines/>, {wrapper: BrowserRouter})
        
        // Make sure all the parts of the Splash page is included
        expect(screen.getByText("Workout Routines")).toBeInTheDocument();
        expect(screen.getByText("A list of all the routines available on Work It Out")).toBeInTheDocument();
        expect(screen.getByText("6 Day Push/Pull/Legs (PPL) Powerbuilding Workout Split & Meal Plan")).toBeInTheDocument();

        console.error = originalError;
    });

});

describe("Test Equipment Page", () => {
    test("Proper Rendering of Equipment Page", async() => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<Equipment/>, {wrapper: BrowserRouter})
        
        // Make sure all the parts of the Splash page is included
        expect(screen.getByText("Equipment")).toBeInTheDocument();
        expect(screen.getByText("A comprehensive list of all the equipment used for the exercises on Work It Out")).toBeInTheDocument();
        expect(screen.getByText("Running Shoes")).toBeInTheDocument();

        console.error = originalError;
    });

});

describe("Test Search Page", () => {
    test("Proper Rendering of Search Page", async() => {
        const originalError = console.error;
        console.error = jest.fn();
        render(<Search/>, {wrapper: BrowserRouter})
        
        // Make sure all the parts of the Splash page is included
        expect(screen.getByText("Search")).toBeInTheDocument();
        expect(screen.getByText("Now sure what you want? Find exercises, routines or equipment related to what you're looking for")).toBeInTheDocument();

        console.error = originalError;
    });

});