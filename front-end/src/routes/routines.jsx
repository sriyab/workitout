import {Container, Row, Col} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css"
import {getRoutines} from "../api-calls/frontend_calls.jsx"
import RoutineCard from "../components/routines/routine_card";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Pagination from 'react-bootstrap/Pagination';
import '../components/pagination.css';
import SearchBar from "../components/search_filter/search_bar.jsx";
import Highlighter from "react-highlight-words";
import Filters from "../components/search_filter/filters";

function Items({ routines, searchText }) {
  return (
    <>
    {routines.map(curr => (
      <RoutineCard
      name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
      author={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.author}/>}
      difficulty={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.difficulty.toString()}/>}
      length={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.length.toString() + ' minutes'}/>}
      type={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.type}/>}
      link={'/routines/'+ curr.id}
      />
      ))}
    </>
  );
}

export default function Routines() {
  const [routines, setRoutines] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const postsPerPage = 9;
  const [searchText, setSearchText] = useState("");
  const [filters, setFilters] = useState({'sort': null, 'difficulty': null, 'type': null})
  const LAYOUT = {
    'sort': {
      'label': 'Sort By:',
      'default': 'none',
      'options': ['name', 'author', 'difficulty', 'length']
    },
    'difficulty': {
      'label': 'Difficulty:',
      'default': 'all',
      'options': ['0', '1', '2']
    },
    'type': {
      'label': 'Type:',
      'default': 'all',
      'options': ['Mobility', 'Cardio', 'Strength']
    }
  }
  
  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const routinesSlice = getRoutines(
        currentPage,
        postsPerPage,
        filters['sort'],
        searchText,
        filters['difficulty'],
        filters['type']
        );
      setRoutines(routinesSlice.Routines);
      setTotalPages(Math.ceil(routinesSlice.meta_total / postsPerPage));
      setLoading(false);
    }
    fetchPosts();
  }, [currentPage, postsPerPage, searchText, filters]);

  const handlePageClick = (event) => {
    console.log(
      `User requested page number ${event.selected}`
    );
    setCurrentPage(event.selected);
  };

  return (
    <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={7}>
        {/* <Image src={"idk"} fluid rounded /> */}
        </Col>
        <Col sm={5}>
        <h1 class="font-weight-light">Workout Routines</h1>
        <p class="mt-4">A list of all the routines available on Work It Out </p>
        </Col>
        <Col sm={6} className="px-4 my-5">
          <SearchBar searchText={searchText} setSearchText={setSearchText} />
        </Col>
        <Col sm={6} className="px-4 my-3">
          <Filters filters={filters} setFilters={setFilters} LAYOUT={LAYOUT} />
        </Col>
    </Row>
    <Row className="px-4 my-5">
      <Items routines={routines} searchText={searchText}/>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <ReactPaginate
          breakLabel={'...'}
          nextLabel={<Pagination.Next />}
          onPageChange={handlePageClick}
          activeClassName={'item active '}
          breakClassName={'item break-me '}
          containerClassName={'pagination'}
          disabledClassName={'disabled-page'}
          marginPagesDisplayed={2}
          nextClassName={"item next "}
          pageClassName={'item pagination-page '}
          pageRangeDisplayed={5}
          previousClassName={"item previous"}
          pageCount={totalPages} 
          previousLabel={<Pagination.Prev /> }
          renderOnZeroPageCount={null} 
        />
      </div>
    </Row>
    </Container>
    </Container>
    </main>
    </div>
  );
  }
