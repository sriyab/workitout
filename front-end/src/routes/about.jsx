import {Container, Card, Row, Col, ListGroup} from "react-bootstrap";
import axios from "axios";
import React from 'react';
import Angela_img from '../images/Angela.webp'
import Goldin_img from '../images/Goldin.webp'
import Sriya_img from '../images/Sriya.webp'
import Vaidehi_img from '../images/Vaidehi.webp'
import Yash_img from '../images/Yash.webp'

const siteDescription = <div>
  <p>
  WorkItOut was created to promote healthier living through physical fitness. 
  We believe that exercise can help improve physical health and reduce illness 
  and injury. Users interested in health and fitness can view different 
  exercises they can perform, what equipment they need, and what illnesses and 
  injuries they are associated with.
  </p>
</div>

const siteResult = <div>
  <p>
    By combining equipment, exercises, and fitness, we can begin to answer 
    questions such as:
  </p>
  <ul>
    <li>How do different groups of exercises differ from each other?</li>
    <li>What injuries and illnesses can certain exercises prevent? What can they cause?</li>
    <li>What is the best equipment to use for exercising certain muscles?</li>
  </ul>
</div>

const siteApis = <div>
  <ul>
    <li>
      <h3><a href='https://www.tredict.com/glossary/'>Tredict</a></h3>
      <p>Information about the equipment used in different exercises will be obtained here (WIP)</p>
    </li>
    <li>
      <h3><a href='https://www.programmableweb.com/api/exercisedb-rest-api-v10/'>ExerciseDB</a></h3>
      <p>Information about individual exercises will be obtained here (WIP)</p>
    </li>
    <li>
      <h3><a href='https://open.cdc.gov/apis.html'>CDC</a></h3>
      <p>General information about health and fitness will be obtained here (WIP)</p>
    </li>
    <li>
      <h3><a href='https://www.healthgraphic.com/discovery'>healthgraphic</a></h3>
      <p>Health related graphics and graphs will be obtained here (WIP)</p>
    </li>
    <li>
      <h3><a href='https://wger.de/en/software/api'>wger</a></h3>
      <p>Information about possible exercise regimens will be obtained here (WIP)</p>
    </li>
    <li>
      <h3><a href='https://docs.gitlab.com/ee/api/'>GitLab</a></h3>
      <p>The GitLab API is used to track individual commit and issue activity</p>
    </li>
  </ul>
</div>

const siteTools = <div>
  <ul>
    <li>
      <h3>React</h3>
      <p>Front-end templating and design using React-Bootstrap, and dynamic 
        webpages using Axios for API requests and React-Router for routing.</p>
    </li>
    <li>
      <h3>Bootstrap</h3>
      <p>Template for design that is responsive to different device media.</p>
    </li>
    <li>
      <h3>Postman</h3>
      <p>Documentation of our site’s RESTful API.</p>
    </li>
    <li>
      <h3>AWS</h3>
      <p>Hosting/serving the website to share with others.</p>
    </li>
  </ul>
</div>

const people = {'Angela Geronimo': 0, 'Goldin Vo': 1, 'Sriya Bandaru': 2, 'Yash Sharma': 3, 'Vaidehi Phirke': 4};
const names = {'angelagmo': 0, 'Goldin Vo': 1, 'Sriya Bandaru': 2, 'sharma.yash.1309': 3, 'Vaidehi Phirke': 4};
const images = [Angela_img, Goldin_img, Sriya_img, Yash_img, Vaidehi_img];
const usernames = ['angelagmo', 'goldinvo', 'sriyab', 'sharma.yash.1309', 'vaidehiphirke']
const roles = ['Front-end Developer', 'Front-end Developer', 'Phase 1 Leader', 'Back-end Developer', 'Back-end Developer'];
const tests = [0, 0, 0, 0, 0];
const bios = [
  "I'm a senior in CS at UT. I moved to Texas from Hawai'i for college, but I'll most likely stay after graduation. I love cats and my hobbies include cooking, playing video games, and helping my friends build mechanical keyboards.",
  "I'm a junior CS major and recently, my hobbies have been making bento boxes, swimming, and embroidering. My favorite fruits are persimmons!",
  "I’m a CS major, and in my free time away from the computer I love to cook, read, and swim. I’ve recently picked up the Ukelele and also started playing soccer!",
  "I am a third year Computer Science student here at UT Austin. I grew up in Florida and moved to the Dallas area in 4th grade. I enjoy playing music on the piano and drums as well as play volleyball in my free time.",
  "Hi! I'm Vaidehi and I'm a junior at UT this year studying Computer Science and Business. I love trying new recipes, going on long walks, watching TV, and learning new things!"
];


function MemberCard(props){
  return(
    <Card style={{ width: '15rem' }}>
      <Card.Img variant="top" src={props.imgPath} />
      <Card.Body>
        <Card.Title>
          {props.role}<br />
        </Card.Title>
        <h3>{props.name}</h3>
        <Card.Text>
        {props.bio}
        </Card.Text>
      </Card.Body>
      <ListGroup className="list-group-flush">
        <ListGroup.Item>Commits: {props.commits}</ListGroup.Item>
        <ListGroup.Item>Issues: {props.issues}</ListGroup.Item>
        <ListGroup.Item>Tests: {props.tests}</ListGroup.Item>
      </ListGroup>
      
    </Card>
    
  )
}

export default function About() {
    const [commitsState, updateCommits] = React.useState({'commits': [0, 0, 0, 0, 0]})
    const [issuesState, updateIssues] = React.useState({'issues': [0, 0, 0, 0, 0]});

    React.useEffect(() => {
      // Commits
      axios.get(
        'https://gitlab.com/api/v4/projects/39770622/repository/contributors'
        ).then((response) => {
          response.data.forEach(record => {
            if (record['name'] in names){
              let i = names[record['name']];
              commitsState['commits'][i] += record['commits'];
              
            }
          });
          updateCommits({...commitsState});
        });
      // Issues
      usernames.forEach((username, index) => {
        axios.get(
          'https://gitlab.com/api/v4/projects/39770622/issues_statistics?assignee_username=' + username
        ).then(response => {
          issuesState['issues'][index] += response.data['statistics']['counts']['closed'];
          updateIssues({...issuesState});
        });
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    let memberCards = [];
    for (let [name, i] of Object.entries(people)) {
      memberCards.push(
        <MemberCard 
          key = {'member_card_' + i}
          name={name}
          imgPath={images[i]}
          bio={bios[i]}
          role={roles[i]}
          commits={commitsState['commits'][i]}
          issues={issuesState['issues'][i]}
          tests={tests[i]}  
        />
      )
    }

    return (
      <Container>
        <Row className="px-4 my-5">
          <Col>
          <h1>About our Site</h1>
          {siteDescription}
          </Col>
          <Col>
          <h1>What's the Result?</h1>
          {siteResult}
          </Col>
        </Row>
        <Row className="px-4 my-5">
          <h1>Members: </h1>
        </Row>
        <Row className="px-4 my-5">
          {memberCards}
          <Col className="px-4 my-4">
          <ul>
            <li>Total number of commits: {commitsState['commits'].reduce((a, b) => a + b, 0)}</li>
            <li>Total number of issues: {issuesState['issues'].reduce((a, b) => a + b, 0)}</li>
            <li>Total number of unit tests: {tests.reduce((a, b) => a + b, 0)}</li>
          </ul>
          </Col>
        </Row>
        <Row className="px-4 my-5">
          <Col>
          <h1>APIs/Data Sources</h1>
          {siteApis}
          </Col>
          <Col>
          <h1>Tools</h1>
          {siteTools}
          </Col>
        </Row>
        <Row className="px-4 my-5">
          <h1>WorkItOut's RESTful API</h1>
          <p>Visit our <a href='https://documenter.getpostman.com/view/23628365/2s83tGoC3s'>API documentation</a></p>
        </Row>
      </Container>
    );
  }