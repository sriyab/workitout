import EquipmentCard from "../components/equipment/equipment_card"
import {Container, Row, Col, Image} from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import {getAllEquipment} from "../api-calls/frontend_calls.jsx"
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate"
import Pagination from 'react-bootstrap/Pagination';
import '../components/pagination.css';
import SearchBar from "../components/search_filter/search_bar";
import Highlighter from "react-highlight-words";
import Filters from "../components/search_filter/filters";

function Items({ equipment , searchText }) {
  return (
    <>
      {equipment.map(curr => (
        <EquipmentCard
        name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
        category={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.category}/>}
        price={<Highlighter searchWords={searchText.split(" ")} textToHighlight={"$" + curr.price_low + " - $" + curr.price_high}/>}
        suitability={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.home_suitable ? 'Yes' : 'No'}/>}
        muscles={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.muscles[0]}/>}
        link={'/equipment/'+ curr.id}
        />
        ))}
    </>
  );
}


export default function Equipment() {
  const [equipment, setEquipment] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const postsPerPage = 9;
  const [searchText, setSearchText] = useState("");
  const [filters, setFilters] = useState({'sort': null, 'home_suitable': null, 'commercial_suitable': null, 'category': null})
  const LAYOUT = {
    'sort': {
      'label': 'Sort By:',
      'default': 'none',
      'options': ['name', 'muscles', 'price', 'category']
    },
    'home_suitable': {
      'label': 'Home Suitable:',
      'default': 'all',
      'options': ['true', 'false']
    },
    'commercial_suitable': {
      'label': 'Commercial Suitable:',
      'default': 'all',
      'options': ['true', 'false']
    },
    'category': {
      'label': 'Category:',
      'default': 'all',
      'options': ['Free Weight', 'Free Weights', 'Accessories', 'All-In-One Machines', 'Resistance Training Machines', 'Cardio (Cardiovascular) Machines']
    },
  }

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const eqiup = getAllEquipment(
        currentPage, 
        postsPerPage, 
        filters['sort'],
        searchText,
        filters['home_suitable'],
        filters['commercial_suitable'],
        null,
        filters['category']
        );
      setEquipment(eqiup.Equipment);
      setTotalPages(Math.ceil(eqiup.meta_total / postsPerPage));
      setLoading(false);
    }
    fetchPosts();
  }, [currentPage, postsPerPage, searchText, filters]);

  const handlePageClick = (event) => {
    console.log(
      `User requested page number ${event.selected}`
    );
    setCurrentPage(event.selected);
  };

  return (
    <div >
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5"> 
        <Col sm={7}>
        </Col>
        <Col sm={5}>
        <h1 class="font-weight-light">Equipment</h1>
        <p class="mt-4">A comprehensive list of all the equipment used for the exercises on Work It Out </p>
        </Col>
        <Col sm={4} className="px-4 my-5">
          <SearchBar searchText={searchText} setSearchText={setSearchText} />
        </Col>
        <Col sm={8} className="px-4 my-3">
          <Filters filters={filters} setFilters={setFilters} LAYOUT={LAYOUT} />
        </Col>
    </Row>
    <Row className="px-4 my-5">
      <Items equipment={equipment} searchText={searchText}/>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
      <ReactPaginate
        breakLabel={'...'}
        nextLabel={<Pagination.Next />}
        onPageChange={handlePageClick}
        activeClassName={'item active '}
        breakClassName={'item break-me '}
        containerClassName={'pagination'}
        disabledClassName={'disabled-page'}
        marginPagesDisplayed={2}
        nextClassName={"item next "}
        pageClassName={'item pagination-page '}
        pageRangeDisplayed={5}
        previousClassName={"item previous"}
        pageCount={totalPages}
        previousLabel={<Pagination.Prev /> }
        renderOnZeroPageCount={null} 
      />
      </div>
    </Row>
    </Container>
    </Container>
    </main>
    </div>
  );
  }