import React from 'react';
import {BarChart, Bar, XAxis, YAxis, PieChart, Pie, Tooltip, Legend, CartesianGrid} from 'recharts';
import {Container, Row, Col} from "react-bootstrap";
import { getPlants, getAnimals, getStates} from "../api-calls/provider_calls";

function ProviderPlants() {
    const plantData = getPlants()['page'];

    let habitatCount = {};
    for (let plant in plantData) {
        let data = plantData[plant];
        const habitats = data['habitats'];
        for (let index in habitats) {
            let habitat = habitats[index];
            if (habitat.length > 0) {
                const currCount = habitatCount[habitat] ?? 0; 
                habitatCount[habitat] = currCount + 1;
            }
        }
    }

    const plantBar = []
    for (let item in habitatCount){
        plantBar.push({habitat: item, count: habitatCount[item]})
    }

    return (
    <BarChart width={1200} height={250} data={plantBar}>
        <XAxis dataKey="habitat" />
        <YAxis />
        <Tooltip />
        <Bar dataKey="count" fill="#54afeb" />
    </BarChart>
    );
}

function ProviderAnimals() {
    const animalData = getAnimals()['page'];

    let threatLevelCount = {};
    for (let animal in animalData) {
        let data = animalData[animal];
        const threatLevel = data['threatLevel'];
        const currCount = threatLevelCount[threatLevel] ?? 0;
        threatLevelCount[threatLevel] = currCount + 1;
    }

    const animalBar = []
    for (let item in threatLevelCount){
        animalBar.push({threatLevel: item, count: threatLevelCount[item]})
    }

    let renderLabel = function(entry) {
        return entry.threatLevel;
    }

    return (
    <PieChart width={750} height={500}>
        <Tooltip />
        <Pie data={animalBar} dataKey="count" label={renderLabel} cx="50%" cy="50%" outerRadius={200} fill="#f5b031" />
    </PieChart>
    );
}

function ProviderStates() {
    const stateData = getStates()['page'];

    const stateBar = []
    for (let index in stateData) {
        let state = stateData[index];
        console.log(state);
        stateBar.push({landArea: state["landArea"], waterArea: state["waterArea"], name: state["abbreviation"]})
    }

    return (
    <BarChart width={1200} height={500} data={stateBar}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="landArea" fill="#82ca9d" />
        <Bar dataKey="waterArea" fill="#27c1d9" />
    </BarChart>
    );
}



export default function ProviderVisualizations() {
    return (
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={6}/>
        <Col sm={6}>
          <h1 class="font-weight-light">EndangeredNature Visualizations</h1>
          <p class="mt-4">Visualizations of data from our provider's API </p>
        </Col>
    </Row>
    <Row className="px-4 my-5">
        <h4>Habitats of Various Plants</h4>
        <ProviderPlants />
        <h4>Threat Level of Animals</h4>
        <ProviderAnimals />
        <h4>Water vs Land Area of Different States</h4>
        <ProviderStates />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    )
}
