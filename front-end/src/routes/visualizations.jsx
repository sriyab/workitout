import React from 'react';
import {ScatterChart, CartesianGrid, Scatter, PieChart, Pie, BarChart, Bar, XAxis, YAxis} from 'recharts';
import {Container, Row, Col} from "react-bootstrap";
import {getExercises, getAllEquipment} from "../api-calls/frontend_calls";

export default function Visualizations() {
    
    // https://stackoverflow.com/questions/56104004/rechart-adding-labels-to-charts
    const renderCustomizedLabel = ({
        x, y, name
      }) => {
        return (
          <text x={x} y={y} fill="black" textAnchor="end" dominantBaseline="central">
            {name}
          </text>
        );
      };

    // get exercise data in format
    // [{category: count}, ...]
    const exerciseData = getExercises()['Exercises'];

    // ===bar chart===
    // dictionary of counts {category: count, ...}
    const exerciseCount = exerciseData.reduce((allExercises, exercise) => {
        const currCount = allExercises[exercise['category']] ?? 0;
        return {
            ...allExercises,
            [exercise['category']]: currCount + 1,
        };
    }, {});
    const exerciseBar = []
    for (let item in exerciseCount){
        exerciseBar.push({category: item, count: exerciseCount[item]})
    }

    // ===pie chart===
    const difficulty_count = exerciseData.reduce((allExercises, exercise) => {
        const currCount = allExercises[exercise['level']] ?? 0;
        return {
            ...allExercises,
            [exercise['level']]: currCount + 1,
        };
    }, {});
    const exercisePie = []
    for (let item in difficulty_count){
        exercisePie.push({category: item, count: difficulty_count[item]})
    }

    // ===scatterplot===
    const equipmentData = getAllEquipment()['Equipment'];

    return (
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={8}/>
        <Col sm={4}>
          <h1 class="font-weight-light">WorkItOut Visualizations</h1>
          <p class="mt-4">Visualizations of data from our own API </p>
        </Col>
    </Row>
    <Row className="px-4 my-5">
        <h5> Categories of Exercises</h5>
        <BarChart width={1000} height={250} data={exerciseBar}>
            <XAxis dataKey="category" />
            <YAxis />
            <Bar dataKey="count" fill="#8884d8" />
        </BarChart>
    </Row>
    <Row className="px-4 my-5">
        <h5> Difficulty of Exercises</h5>
        <PieChart width={400} height={400}>
            <Pie data={exercisePie} dataKey='count' nameKey="category" cx="50%" cy="50%" fill="#8884d8" label={renderCustomizedLabel} labelLine={false}/>
        </PieChart>
    </Row>
    <Row className="px-4 my-5">
        <h5> Low price versus High price of Eqiupment</h5>
        <ScatterChart
          width={400}
          height={400}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="price_low" label={{ value: "Low Price", dy: 25}}/>
          <YAxis type="number" dataKey="price_high" label={{ value: "High Price", angle: -90, dx: -35}}/>
          <Scatter  data={equipmentData} fill="#8884d8" />
        </ScatterChart>
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    )
}
