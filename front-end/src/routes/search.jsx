import ExerciseCard from "../components/exercises/exercise_card";
import RoutineCard from "../components/routines/routine_card";
import EquipmentCard from "../components/equipment/equipment_card";
import {Container, Row, Col} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css"
import {getExercises, getAllEquipment, getRoutines} from "../api-calls/frontend_calls.jsx"
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Pagination from 'react-bootstrap/Pagination';
import '../components/pagination.css';
import SearchBar from "../components/search_filter/search_bar";
import Highlighter from "react-highlight-words";

function Items({ exercises, equipment, routines, searchText }) {
    return (
        <>
        <div><h3>Exercises</h3></div>
        {exercises.map(curr => (
        <ExerciseCard
        name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
        category={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.category}/>}
        level={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.level}/>}
        force={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.force}/>}
        primary_muscles={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.muscles_primary[0]}/>}
        equipment={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.equipment_name}/>}
        link={'/exercises/'+ curr.id}
        />
        ))}
        <div><h3>Routines</h3></div>
        {routines.map(curr => (
          <RoutineCard
          name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
          author={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.author}/>}
          difficulty={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.difficulty.toString()}/>}
          length={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.length.toString() + ' minutes'}/>}
          type={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.type}/>}
          link={'/routines/'+ curr.id}
          />
          ))}
        <div><h3>Equipment</h3></div>
        {equipment.map(curr => (
          <EquipmentCard
          name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
          category={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.category}/>}
          price={<Highlighter searchWords={searchText.split(" ")} textToHighlight={"$" + curr.price_low + " - $" + curr.price_high}/>}
          suitability={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.home_suitable ? 'Yes' : 'No'}/>}
          muscles={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.muscles[0]}/>}
          link={'/equipment/'+ curr.id}
          />
          ))}
        </>
    );
}
 
export default function Search() {
    const [exercises, setExercises] = useState([]);
    const [equipment, setEquipment] = useState([]);
    const [routines, setRoutines] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const postsPerPage = 3;
    const [searchText, setSearchText] = useState("");

    useEffect(() => {
        const fetchPosts = async () => {
            setLoading(true);
            const exercisesSlice = getExercises(currentPage, postsPerPage, null, searchText);
            setExercises(exercisesSlice.Exercises);
            const routinesSlice = getRoutines(currentPage, postsPerPage, null, searchText);
            setRoutines(routinesSlice.Routines);
            const eqiup = getAllEquipment(currentPage, postsPerPage, null, searchText);
            setEquipment(eqiup.Equipment);
            setTotalPages(Math.ceil((exercisesSlice.meta_total + routinesSlice.meta_total + eqiup.meta_total)/ postsPerPage) + 1);
            setLoading(false);
        }
        fetchPosts();
    }, [currentPage, postsPerPage, searchText]);

    const handlePageClick = (event) => {
        console.log(
        `User requested page number ${event.selected}`
        );
        setCurrentPage(event.selected);
    };

    return (
        <div>
        <main>
        <Container>
        <Container>
        <Row className="px-4 my-5">
            <Col sm ={7}>
            {/* <Image src={"idk"} fluid rounded /> */}
            </Col>
            <Col sm={5}>
                <h1 class="font-weight-light">Search</h1>
                <p class="mt-4">Now sure what you want? Find exercises, routines or equipment related to what you're looking for </p>
                {/* <Link className='btn btn-primary' role='button' to='about'>About Us</Link> */}
                <SearchBar searchText={searchText} setSearchText={setSearchText} />
            </Col>
        </Row>
        <Row className="px-4 my-5">
        <Items exercises={exercises} equipment={equipment} routines={routines} searchText={searchText}/>
        <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
        <ReactPaginate
          breakLabel={'...'}
          nextLabel={<Pagination.Next />}
          onPageChange={handlePageClick}
          activeClassName={'item active '}
          breakClassName={'item break-me '}
          containerClassName={'pagination'}
          disabledClassName={'disabled-page'}
          marginPagesDisplayed={2}
          nextClassName={"item next "}
          pageClassName={'item pagination-page '}
          pageRangeDisplayed={5}
          previousClassName={"item previous"}
          pageCount={totalPages}
          previousLabel={<Pagination.Prev /> }
          renderOnZeroPageCount={null} 
        />
      </div>
    </Row>
        </Container>
        </Container>
        </main>
        </div>
      );
}
