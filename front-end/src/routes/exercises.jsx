import ExerciseCard from "../components/exercises/exercise_card";
import {Container, Row, Col} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css"
import {getExercises} from "../api-calls/frontend_calls.jsx"
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Pagination from 'react-bootstrap/Pagination';
import '../components/pagination.css';
import SearchBar from "../components/search_filter/search_bar";
import Filters from "../components/search_filter/filters";
import Highlighter from "react-highlight-words";

function Items({ exercises, searchText }) {
  return (
    <>
    {exercises.map(curr => (
      <ExerciseCard
      name={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.name}/>}
      category={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.category}/>}
      level={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.level}/>}
      force={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.force}/>}
      primary_muscles={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.muscles_primary[0]}/>}
      equipment={<Highlighter searchWords={searchText.split(" ")} textToHighlight={curr.equipment_name}/>}
      link={'/exercises/'+ curr.id}
      />
    ))}
    </>
  );
}

export default function Exercises() {
  const [exercises, setExercises] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const postsPerPage = 9;
  const [searchText, setSearchText] = useState("");
  const [filters, setFilters] = useState({'sort': null, 'force': null, 'level': null, 'mechanic': null, 'category': null})
  const LAYOUT = {
    'sort': {
      'label': 'Sort By:',
      'default': 'none',
      'options': ['name', 'force', 'level', 'mechanic', 'category']
    },
    'force': {
      'label': 'Force:',
      'default': 'all',
      'options': ['static', 'push', 'pull']
    },
    'level': {
      'label': 'Level:',
      'default': 'all',
      'options': ['beginner', 'intermediate', 'expert']
    },
    'mechanic': {
      'label': 'Mechanic:',
      'default': 'all',
      'options': ['isolation', 'compound']
    },
    'category': {
      'label': 'Category:',
      'default': 'all',
      'options': ['cardio', 'strongman', 'powerlifting', 'plyometrics', 'stretching', 'olympic weightlifting', 'strength']
    }
  }

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const exercisesSlice = getExercises(
        currentPage, 
        postsPerPage, 
        filters['sort'],
        searchText,
        filters['force'],
        filters['level'],
        filters['mechanic'],
        filters['category'],
        );
      setExercises(exercisesSlice.Exercises);
      setTotalPages(Math.ceil(exercisesSlice.meta_total / postsPerPage));
      setLoading(false);
    }
    fetchPosts();
  }, [currentPage, postsPerPage, searchText, filters]);

  const handlePageClick = (event) => {
    console.log(
      `User requested page number ${event.selected}`
    );
    setCurrentPage(event.selected);
  };

  return (
    
    <div>
    <main>
    <Container>
    <Container>
    <div>
    </div>
    <Row className="px-4 my-5">
        <Col sm={7}>
        {/* <Image src={"idk"} fluid rounded /> */}
        </Col>
        <Col sm={5}>
          <h1 class="font-weight-light">Exercises</h1>
          <p class="mt-4">A list of all the exercises on Work It Out </p>
        </Col>
        <Col sm={5} className="px-4 my-5">
          <SearchBar searchText={searchText} setSearchText={setSearchText} />
        </Col>
        <Col sm={7} className="px-4 my-3">
          <Filters filters={filters} setFilters={setFilters} LAYOUT={LAYOUT} />
        </Col>
    </Row>
    <Row className="px-4 my-5">
        <Items exercises={exercises} searchText = {searchText} />
        <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
        <ReactPaginate
          breakLabel={'...'}
          nextLabel={<Pagination.Next />}
          onPageChange={handlePageClick}
          activeClassName={'item active '}
          breakClassName={'item break-me '}
          containerClassName={'pagination'}
          disabledClassName={'disabled-page'}
          marginPagesDisplayed={2}
          nextClassName={"item next "}
          pageClassName={'item pagination-page '}
          pageRangeDisplayed={5}
          previousClassName={"item previous"}
          pageCount={totalPages}
          previousLabel={<Pagination.Prev /> }
          renderOnZeroPageCount={null} 
        />
      </div>
    </Row>
    </Container>
    </Container>
    </main>
    </div>
  );
  }