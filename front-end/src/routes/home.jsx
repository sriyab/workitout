import ExercisesCard from "../components/home/exercises_card"
import EquipmentCard from "../components/home/equipment_card"
import RoutinesCard from "../components/home/routines_card"
import {Container, Row, Col, Image} from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import home_page from "../images/home_page.webp"
import { Link } from 'react-router-dom';

export default function Home() {
  return (
    <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={7}>
        <Image src={home_page} fluid rounded />
        </Col>
        <Col sm={5}>
        <h1 class="font-weight-light">Welcome to WorkItOut!</h1>
        <p class="mt-4">We're a team committed to helping people improve their physical health and reduce illness and injury through exercise! </p>
        <Link className='btn btn-primary' role='button' to='about'>About Us</Link>
        </Col>
    </Row>
    <Row className="px-4 my-5">
        <ExercisesCard/>
        <RoutinesCard/>
        <EquipmentCard/>
    </Row>
    </Container>
    </Container>
    </main>
    </div>
  );
}
