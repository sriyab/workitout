import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import {
//   createBrowserRouter,
//   RouterProvider,
//   Route,
// } from "react-router-dom";
// import Root from "./routes/root";
// import ErrorPage from "./error-page";
// import About from "./routes/about"
// import Equipment from "./routes/equipment"
// import Exercises from "./routes/exercises"
// import Injury from "./routes/injury"
// import injury_grid_page from "./injuries/injury_grid_page"
// import Exercises from "../routes/exercises";



// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <Root />,
//     errorElement: <ErrorPage />,
//   },
//   {
//     path: "about",
//     element: <About />,
//   },
//   {
//     path: "equipment",
//     element: <Equipment />,
//   },
//   {
//     path: "exercises",
//     element: <Exercises />,
//   },
//   {
//     path: "injury",
//     element: <injury_grid_page />,
//   },
// ]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
    <App/>
    //{/* <RouterProvider router={router} /> */}
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
