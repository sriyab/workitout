const deployLink = "https://api.workit0ut.me"; //new api
const localLink = "http://127.0.0.1:5000";
const testing = false;

export function getExercise(id) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/exercises/${id}`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}
  
export function getExercises(page, limit, sort, search, force, level, mechanic, category) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  let url = `${link}/exercises?`;

  if (page != null) {
    url += `&page=${page}`;
  }
  if (limit != null) {
    url += `&limit=${limit}`;
  }
  if (sort != null) {
    url += `&sort=${sort}`;
  }
  if (search != null) {
    url += `&search=${search}`;
  }
  if (force != null) {
    url += `&force=${force}`;
  }
  if (level != null) {
    url += `&level=${level}`;
  }
  if (mechanic != null) {
    url += `&mechanic=${mechanic}`;
  }
  if (category != null) {
    url += `&category=${category}`;
  }
  request.open("GET", url, false); 
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}

export function getEquipment(id) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/equipment/${id}`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}
  
export function getAllEquipment(page, limit, sort, search, home_suitable, commercial_suitable, muscles, category) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  let url = `${link}/equipment?`;

  if (page != null) {
    url += `&page=${page}`;
  }
  if (limit != null) {
    url += `&limit=${limit}`;
  }
  if (sort != null) {
    url += `&sort=${sort}`;
  }
  if (search != null) {
    url += `&search=${search}`;
  }
  if (commercial_suitable != null) {
    url += `&commercial_suitable=${commercial_suitable}`;
  }
  if (home_suitable != null) {
    url += `&home_suitable=${home_suitable}`;
  }
  if (muscles != null) {
    url += `&muscles=${muscles}`;
  }
  if (category != null) {
    url += `&category=${category}`;
  }
  request.open("GET", url, false); 
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}

export function getRoutine(id) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/routines/${id}`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}
  
export function getRoutines(page, limit, sort, search, difficulty, type) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  let url = `${link}/routines?`;

  if (page != null) {
    url += `&page=${page}`;
  }
  if (limit != null) {
    url += `&limit=${limit}`;
  }
  if (sort != null) {
    url += `&sort=${sort}`;
  }
  if (search != null) {
    url += `&search=${search}`;
  }
  if (difficulty != null) {
    url += `&difficulty=${difficulty}`;
  }
  if (type != null) {
    url += `&type=${type}`;
  }

  request.open("GET", url, false); 
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}

// RELATED CALLS
// return type for all:
    // {
    //   related1: [],
    //   related2: []
    // }
// param obj is useParams() -> so the whole instance object {}
export function getExercisesRelated(obj) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/exercises/${obj.exerciseID}/related`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}

export function getEquipmentRelated(obj) {
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/equipment/${obj.equipmentID}/related`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}

export function getRoutinesRelated(obj) {
  // method 1: obj contains list of ids so might as well use them..
  // let result = {
  //   exercises: [],
  //   equipment: []
  // };
  // result.exercises.push(...obj.exercises);
  // result.equipment.push(...obj.equipment);
  // return result;
  
  // method 2: access the api 'related' endpoint
  const request = new XMLHttpRequest();
  const link = testing ? localLink : deployLink;
  request.open("GET", `${link}/routines/${obj.routineID}/related`, false);
  request.setRequestHeader("Accept", "application/vnd.api+json");
  request.send();
  let response = null;
  if (request.status === 200) {
    response = JSON.parse(request.responseText);
  }
  return response;
}