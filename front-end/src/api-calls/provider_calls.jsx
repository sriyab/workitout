const link = "https://api.endangerednature.me";

export function getPlants() {
  try {
    const request = new XMLHttpRequest();
    request.open("GET", `${link}/plants?perPage=100000`, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send();
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    return response;
  } catch (error) {
    getPlants();
  }
}

export function getAnimals() {
  try {
    const request = new XMLHttpRequest();
    request.open("GET", `${link}/animals?perPage=100000`, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send();
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    return response;
  } catch (error) {
    getAnimals();
  }
}

export function getStates() {
  try {
    const request = new XMLHttpRequest();
    request.open("GET", `${link}/states?perPage=100000`, false);
    request.setRequestHeader("Accept", "application/vnd.api+json");
    request.send();
    let response = null;
    if (request.status === 200) {
      response = JSON.parse(request.responseText);
    }
    return response;
  } catch (error) {
    getStates();
  }
}
