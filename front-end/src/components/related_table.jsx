import Table from 'react-bootstrap/Table';

export default function RelatedTable(props) {
    var exercises = props.exercises;
    var exercisesMap = exercises.map(function(exercises){
        return <li>{exercises}</li>;
        })
  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>Type</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>Mark</td>
        </tr>
        <tr>
          <td>2</td>
          <td>Jacob</td>
        </tr>
        <tr>
          <td>3</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </Table>
  );
}
