import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import React from "react";
import { Typography } from '@mui/material';

//  Props {
//   image?: any;
//   href?: string;
//   attribute1?: String;
//   attribute2?: String;
//   attribute3?: String;
//   attribute4?: String;
//   attribute5?: String;
// }


function ModelCard(props) {
  var image = "holder.js/100px180?text=Image cap"; //props.image? props.image : "holder.js/100px180?text=Image cap";
  return (
     
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text>
          {props.text}
        </Card.Text>
      </Card.Body>
      <Typography>
        {props.attribute1}
      </Typography>
      <ListGroup className="list-group-flush">
        <ListGroup.Item>{props.attribute1}</ListGroup.Item>
        <ListGroup.Item>{props.attribute2}</ListGroup.Item>
        <ListGroup.Item>{props.attribute3}</ListGroup.Item>
        <ListGroup.Item>{props.attribute4}</ListGroup.Item>
        <ListGroup.Item>{props.attribute5}</ListGroup.Item>
      </ListGroup>
      <Card.Body>
        {/* <Button  href={props.buttonLink}>{props.buttonText}</Button> */}
      </Card.Body>
    </Card>
  );
}

export default ModelCard;