import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import home_routines from "../../images/home_routines.jpg"
import { Link } from 'react-router-dom';

function RoutinesCard() {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        <Card.Img variant="top" src={home_routines} />
        <Card.Body>
          <Card.Title>Workout Routines</Card.Title>
          <Card.Text>
            A list of workout routines with relevant information, such as:
          </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Author</ListGroup.Item>
          <ListGroup.Item>Difficulty</ListGroup.Item>
          <ListGroup.Item>Length</ListGroup.Item>
          <ListGroup.Item>Type</ListGroup.Item>
          <ListGroup.Item>Link</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='routines'>See All Routines</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default RoutinesCard;
