import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import home_exercise from "../../images/home_exercise.webp"
import { Link } from 'react-router-dom';

function ExercisesCard() {
  return (
    <Card className='m-5' style={{ width: '20rem' }}>
      <Card.Img variant="top" src={home_exercise} />
      <Card.Body>
        <Card.Title>Exercises</Card.Title>
        <Card.Text>
          A list of exercises with relevant information, such as:
        </Card.Text>
      </Card.Body>
      <ListGroup className="list-group-flush">
        <ListGroup.Item>How to Perform</ListGroup.Item>
        <ListGroup.Item>Duration</ListGroup.Item>
        <ListGroup.Item>Sets and Reps</ListGroup.Item>
        <ListGroup.Item>Effort</ListGroup.Item>
        <ListGroup.Item>Muscle Group</ListGroup.Item>
      </ListGroup>
      <Card.Body>
        <Link className='btn btn-primary' role='button' to='exercises'>See All Exercises</Link>
      </Card.Body>
    </Card>
  );
}

export default ExercisesCard;



  
