import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import home_equipment from "../../images/home_equipment.webp"
import { Link } from 'react-router-dom';

function EquipmentCard() {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        <Card.Img variant="top" src={home_equipment} />
        <Card.Body>
          <Card.Title>Equipment</Card.Title>
          <Card.Text>
            A list of equipment used for exercises on this site, with relevant information such as:
          </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Exercise Type</ListGroup.Item>
          <ListGroup.Item>Equipment Type</ListGroup.Item>
          <ListGroup.Item>Cost</ListGroup.Item>
          <ListGroup.Item>Workouts</ListGroup.Item>
          <ListGroup.Item>Usage</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='equipment'>See All Equipment</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default EquipmentCard;
