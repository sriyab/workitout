import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Link } from 'react-router-dom';

function EquipmentCard(props) {
    // const buttonLink = '/equipment/' + {props.id};
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
        <ListGroup.Item><b>Category:</b> {props.category}</ListGroup.Item>
          <ListGroup.Item><b>Muscles Used:</b> {props.muscles}</ListGroup.Item>
          <ListGroup.Item><b>Suitability:</b> {props.suitability}</ListGroup.Item>
          <ListGroup.Item><b>Price:</b> {props.price}</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to={props.link}>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default EquipmentCard;