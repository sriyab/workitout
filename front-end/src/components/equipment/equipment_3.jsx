import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard1 from "../exercises/exercise_card_1";

export default function Equipment3() {
    return(
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            <Image src="https://www.pennlive.com/resizer/87lvKqaCqQj_WAhc_o1fJ5muVKo=/1280x0/smart/cloudfront-us-east-1.images.arcpublishing.com/advancelocal/TP7X7F7JSVEKDGRIUKPAL4ENEI.png" fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">Treadmill</h1>
            <p class="mt-4">A device having an endless belt on which an individual walks or runs in place for exercise.</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Exercise Type: Cardio</ListGroup.Item>
                <ListGroup.Item>Equipment Type: Free</ListGroup.Item>
                <ListGroup.Item>Cost: $500-$1500</ListGroup.Item>
                <ListGroup.Item>Workouts: Daily Cardio</ListGroup.Item>
                <ListGroup.Item>Usage: Set speed and incline, then get on the track and run or walk</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/BwuaiNWBodo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        <ExerciseCard1 />
        <ExerciseCard1 />
        <ExerciseCard1 />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}