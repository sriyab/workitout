import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

// import home_equipment from "../../images/home_equipment.jpg"

function EquipmentCard3() {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={home_equipment} /> */}
        <Card.Body>
          <Card.Title>Treadmill</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Exercise Type: Cardio</ListGroup.Item>
          <ListGroup.Item>Equipment Type: Free</ListGroup.Item>
          <ListGroup.Item>Cost: $500-$1500</ListGroup.Item>
          <ListGroup.Item>Workouts: Daily Cardio</ListGroup.Item>
          <ListGroup.Item>Usage: Set speed and incline, then get on the track and run or walk</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='/equipment/eq3'>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default EquipmentCard3;