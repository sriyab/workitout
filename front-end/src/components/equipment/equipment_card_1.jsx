import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Link } from 'react-router-dom';
// import home_equipment from "../../images/home_equipment.jpg"

function EquipmentCard1() {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={home_equipment} /> */}
        <Card.Body>
          <Card.Title>Dumbbells</Card.Title>
          {/* <Card.Text>
            A list of equipment used for exercises on this site, with relevant information such as:
          </Card.Text> */}
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Exercise Type: Weightlifting</ListGroup.Item>
          <ListGroup.Item>Equipment Type: Weight</ListGroup.Item>
          <ListGroup.Item>Cost: $20-$200</ListGroup.Item>
          <ListGroup.Item>Workouts: Dumbbell Rows</ListGroup.Item>
          <ListGroup.Item>Usage: Lift repeatedly in manner indicated by exercise</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='/equipment/eq1'>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default EquipmentCard1;