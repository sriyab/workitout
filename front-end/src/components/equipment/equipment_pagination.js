import React from 'react'
import EquipmentCard from './equipment_card'

export const equipment_pagination = ({ equipment, loading}) => {
    if(loading) {
        return <h2>Loading...</h2>
    }
  return (
    <div>
    <ul className="list-group mb-4">
        {equipment.map(curr => (
            <li key={curr.id} className="list-group-item">
                <EquipmentCard props={curr}/>
            </li>
        ))}
    </ul>
    </div>
  )
}
