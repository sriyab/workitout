import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

// import home_equipment from "../../images/home_equipment.jpg"

function EquipmentCard2() {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={home_equipment} /> */}
        <Card.Body>
          <Card.Title>Jump Rope</Card.Title>
          {/* <Card.Text>
            A list of equipment used for exercises on this site, with relevant information such as:
          </Card.Text> */}
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Exercise Type: Cardio</ListGroup.Item>
          <ListGroup.Item>Equipment Type: Free</ListGroup.Item>
          <ListGroup.Item>Cost: $10-$50</ListGroup.Item>
          <ListGroup.Item>Workouts: None yet!</ListGroup.Item>
          <ListGroup.Item>Usage: Jump while twirling rope</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='/equipment/eq2'>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default EquipmentCard2;