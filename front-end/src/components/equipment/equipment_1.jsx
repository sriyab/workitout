import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard1 from "../exercises/exercise_card_1";
import ExerciseCard2 from "../exercises/exercise_card_2";
import ExerciseCard3 from "../exercises/exercise_card_3";

export default function Equipment1() {
    return(
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            <Image src="https://www.gannett-cdn.com/presto/2020/08/31/USAT/57ea2bda-0aab-485b-8046-86c7fa02f072-Dumbbells.png?width=660&height=372&fit=crop&format=pjpg&auto=webp" fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">Dumbbells</h1>
            <p class="mt-4">A dumbbell is a short bar with weights on either side which people use for physical exercise to strengthen their arm and shoulder muscles.</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Exercise Type: Weightlifting</ListGroup.Item>
                <ListGroup.Item>Equipment Type: Weight</ListGroup.Item>
                <ListGroup.Item>Cost: $20-$200</ListGroup.Item>
                <ListGroup.Item>Workouts: Dumbbell Rows</ListGroup.Item>
                <ListGroup.Item>Usage: Lift repeatedly in manner indicated by exercise</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/WDNQk_vJ2xE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        <ExerciseCard1 />
        <ExerciseCard2 />
        <ExerciseCard3 />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}