import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard1 from "../exercises/exercise_card_1";

export default function Equipment2() {
    return(
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            <Image src="https://cdn.thewirecutter.com/wp-content/uploads/2017/09/jumpropes-lowres-5921.jpg?auto=webp&quality=60&width=570" fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">Jump Rope</h1>
            <p class="mt-4">A rope used for exercises and children's games that involve jumping over the usually twirling rope each time it reaches its lowest point.</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Exercise Type: Cardio</ListGroup.Item>
                <ListGroup.Item>Equipment Type: Free</ListGroup.Item>
                <ListGroup.Item>Cost: $10-$50</ListGroup.Item>
                <ListGroup.Item>Workouts: None yet!</ListGroup.Item>
                <ListGroup.Item>Usage: Jump while twirling rope</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/u3zgHI8QnqE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        <ExerciseCard1 />
        <ExerciseCard1 />
        <ExerciseCard1 />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}