import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard from "../exercises/exercise_card";
import EquipmentCard from "../equipment/equipment_card";
import RoutineCard from "../routines/routine_card";
import {getEquipment, getEquipmentRelated, getExercise, getRoutine} from "../../api-calls/frontend_calls.jsx"

function RelatedRoutineCards({ relatedRoutines }) {
    return (
        <>
          {relatedRoutines?.map(curr => (
            <RoutineCard
            name={curr.name}
            author={curr.author}
            difficulty={curr.difficulty.toString()}
            length={curr.length.toString() + ' minutes'}
            type={curr.type}
            link={'/routines/'+ curr.id}
            />
            ))}
        </>
      );
}

function RelatedExerciseCards({ relatedExercises }) {
    return (
        <>
          {relatedExercises?.map(curr => (
            <ExerciseCard
            name={curr.name}
            category={curr.category}
            level={curr.level}
            force={curr.force}
            primary_muscles={curr.muscles_primary}
            equipment={curr.equipment_name}
            link={'/exercises/'+ curr.id}
            />
            ))}
        </>
      );
}

export default function EquipmentPage() {
    const id = useParams().equipmentID;
    const equipment = getEquipment(id);
    const relatedIDs = getEquipmentRelated(useParams());

    if(equipment == null) {
        return <h2>404: Page Not Found</h2>;
    }

    var relatedRoutines = [];
    var relatedExercises = [];

    for (let index in relatedIDs['routines']) {
        const rID = relatedIDs['routines'][index];
        relatedRoutines.push(getRoutine(rID));
    }

    for (let index in relatedIDs['exercises']) {
        const exID = relatedIDs['exercises'][index];
        relatedExercises.push(getExercise(exID));
    }

    return(
        <div>
        <main>
        <Container>
        <Container>
        <Row className="px-4 my-5">
            <Col sm={4}>
                <Image src={equipment.images[0]} fluid rounded />
            </Col>
            <Col sm={4}>
                <h1 class="font-weight-light">{equipment.name}</h1>
                <p class="mt-4">{equipment.description}</p>
            </Col>
            <Col sm={4}>
                <ListGroup>
                    <ListGroup.Item>Category: {equipment.category}</ListGroup.Item>
                    <ListGroup.Item>Muscles Used: {equipment.muscles}</ListGroup.Item>
                    <ListGroup.Item>Suitability: {equipment.home_suitable}</ListGroup.Item>
                    <ListGroup.Item>Price: {equipment.price_low} - {equipment.price_high}</ListGroup.Item>
                    <ListGroup.Item>Tips: {equipment.tips}</ListGroup.Item>
                </ListGroup>
            </Col>
        </Row>
        <Row>
            {/* consider changing styles */}
            <iframe width="560" height="315" src={equipment.videos[0]} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </Row>
        <Row className="px-4 my-5">
            <h3>Related</h3>
        </Row>
        <Row className="px-4 my-5">
            {<RelatedRoutineCards relatedRoutines={relatedRoutines}/>}
            {<RelatedExerciseCards relatedExercises={relatedExercises}/>}
        </Row>
        </Container>
        </Container>
        </main>
        </div>
        );
}
