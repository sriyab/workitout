import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";


function Navigation() {
  return (
      <Navbar bg="dark" variant="dark" expand="lg">
        <Container>
            <Navbar.Brand  as={Link} to="/">Work It Out</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                <Nav.Link as={Link} to="/exercises">Exercises</Nav.Link>
                <Nav.Link as={Link} to="/equipment">Equipment</Nav.Link>
                <Nav.Link as={Link} to="/routines">Routines</Nav.Link>
                <Nav.Link as={Link} to="/search">Search</Nav.Link>
                <Nav.Link as={Link} to="/about">About</Nav.Link>
                <Nav.Link as={Link} to="/visualizations">Visualizations</Nav.Link>
                <Nav.Link as={Link} to="/provider_visualizations">Provider Visualizations</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
      </Navbar>
  );
}

export default Navigation;