import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard from "../exercises/exercise_card";
import EquipmentCard from "../equipment/equipment_card";
import {getRoutine, getEquipment, getExercise, getRoutinesRelated} from "../../api-calls/frontend_calls.jsx"

function RelatedEquipmentCards({ relatedEquipment }) {
    return (
        <>
          {relatedEquipment?.map(curr => (
            <EquipmentCard
            name={curr.name}
            category={curr.category}
            price={"$" + curr.price_low + " - $" + curr.price_high}
            suitability={curr.home_suitable ? 'Yes' : 'No'}
            muscles={curr.muscles}
            link={'/equipment/'+ curr.id}
            />
            ))}
        </>
      );
  }

  function RelatedExerciseCards({ relatedExercises }) {
    return (
        <>
          {relatedExercises?.map(curr => (
            <ExerciseCard
            name={curr.name}
            category={curr.category}
            level={curr.level}
            force={curr.force}
            primary_muscles={curr.muscles_primary}
            equipment={curr.equipment_name}
            link={'/exercises/'+ curr.id}
            />
            ))}
        </>
      );
  }

export default function RoutinePage() {
    const id = useParams().routineID;
    const routine = getRoutine(id);
    const relatedIDs = getRoutinesRelated(useParams());

    if(routine == null) {
        return <h2>404: Page Not Found</h2>;
    }

    var relatedEquipment = [];
    var relatedExercises = [];

    for (let index in relatedIDs['equipment']) {
        const eqID = relatedIDs['equipment'][index];
        relatedEquipment.push(getEquipment(eqID));
    }

    for (let index in relatedIDs['exercises']) {
        const exID = relatedIDs['exercises'][index];
        relatedExercises.push(getExercise(exID));
    }

    // just borrow media from somewhere else ¯\_(ツ)_/¯
    const image = relatedExercises[0]['images'][0]
    const video = relatedExercises[0]['videos'][0]

    return(
    <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            <Image src={image} fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">{routine.name}</h1>
            <p class="mt-4">{routine.overview}</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Author: {routine.author}</ListGroup.Item>
                <ListGroup.Item>Difficulty: {routine.difficulty}</ListGroup.Item>
                <ListGroup.Item>Length: {routine.length}</ListGroup.Item>
                <ListGroup.Item>Type: {routine.type}</ListGroup.Item>
                <ListGroup.Item>Link: {routine.link ? routine.link : "None"}</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src={video} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        {<RelatedEquipmentCards relatedEquipment={relatedEquipment}/>}
        {<RelatedExerciseCards relatedExercises={relatedExercises}/>}
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}
