import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Link } from 'react-router-dom';

function RoutineCard(props) {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item><b>Author:</b> {props.author}</ListGroup.Item>
          <ListGroup.Item><b>Difficulty:</b> {props.difficulty}</ListGroup.Item>
          <ListGroup.Item><b>Length:</b> {props.length}</ListGroup.Item>
          <ListGroup.Item><b>Type:</b> {props.type}</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to={props.link}>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default RoutineCard;