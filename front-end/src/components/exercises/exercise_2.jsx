import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import EquipmentCard1 from "../equipment/equipment_card_1";

export default function Exercise2() {
    return(
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            {/* <Image src="https://wger.de/media/exercise-images/81/Biceps-curl-1.png" fluid rounded /> */}
            <Image src="http://d205bpvrqc9yn1.cloudfront.net/0294.gif" fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">Biceps Curl with Dumbbell</h1>
            <p class="mt-4">Hold two barbells, the arms are streched, the hands are on your side, the palms face inwards. Bend the arms and bring the weight with a fast movement up. At the same time, rotate your arms by 90 degrees at the very beginning of the movement. At the highest point, rotate a little the weights further outwards. Without a pause, bring them down, slowly. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Muscle Group: Biceps</ListGroup.Item>
                <ListGroup.Item>Effort: Medium</ListGroup.Item>
                <ListGroup.Item>Sets: 4</ListGroup.Item>
                <ListGroup.Item>Reps: 10</ListGroup.Item>
                <ListGroup.Item>Duration: 5 min</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/XE_pHwbst04" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        <EquipmentCard1 />
        <EquipmentCard1 />
        <EquipmentCard1 />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}