import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import exercise_squat from '../../images/exercise_squat.jpg';
import { Link } from 'react-router-dom';
 
function ExerciseCard1() {
    return(
        <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={exercise_squat} /> */}
        <Card.Body>
          <Card.Title>Dumbell Triceps Extension</Card.Title>
          {/* <Card.Text>
            An exercise
          </Card.Text> */}
          {/* id 0430 in rapidapi */}
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Muscle Group: Triceps</ListGroup.Item>
          <ListGroup.Item>Effort: Low</ListGroup.Item>
          <ListGroup.Item>Sets: 4</ListGroup.Item>
          <ListGroup.Item>Reps: 10</ListGroup.Item>
          <ListGroup.Item>Duration: 5 min</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to='/exercises/ex1'>See More</Link>
        </Card.Body>
      </Card>
    );
}

export default ExerciseCard1;