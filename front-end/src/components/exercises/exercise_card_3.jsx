import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import exercise_squat from '../../images/exercise_squat.jpg';
import { Link } from 'react-router-dom';

function ExerciseCard3() {
    return(
        <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={exercise_squat} /> */}
        <Card.Body>
          <Card.Title>Barbell Sumo Deadlift</Card.Title>
          {/* <Card.Text>
            An exercise
          </Card.Text> */}
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Muscle Group: Quads</ListGroup.Item>
          <ListGroup.Item>Effort: High</ListGroup.Item>
          <ListGroup.Item>Sets: 3</ListGroup.Item>
          <ListGroup.Item>Reps: 8</ListGroup.Item>
          <ListGroup.Item>Duration: 4 min</ListGroup.Item>
        </ListGroup>
        <Card.Body>
        <Link className='btn btn-primary' role='button' to='/exercises/ex3'>See More</Link>
        </Card.Body>
      </Card>
    );
}

export default ExerciseCard3;