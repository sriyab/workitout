import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import EquipmentCard1 from "../equipment/equipment_card_1";

export default function Exercise3() {
    return(
        <div>
        <main>
        <Container>
        <Container>
        <Row className="px-4 my-5">
            <Col sm={4}>
                <Image src="http://d205bpvrqc9yn1.cloudfront.net/0117.gif" fluid rounded />
            </Col>
            <Col sm={4}>
                <h1 class="font-weight-light">Barbell Sumo Deadlift</h1>
                <p class="mt-4">Begin with a bar loaded on the ground. Approach the bar so that the bar intersects the middle of the feet. The feet should be set very wide, near the collars. Bend at the hips to grip the bar. The arms should be directly below the shoulders, inside the legs, and you can use a pronated grip, a mixed grip, or hook grip. Relax the shoulders, which in effect lengthens your arms. Take a breath, and then lower your hips, looking forward with your head with your chest up. Drive through the floor, spreading your feet apart, with your weight on the back half of your feet. Extend through the hips and knees. As the bar passes through the knees, lean back and drive the hips into the bar, pulling your shoulder blades together. Return the weight to the ground by bending at the hips and controlling the weight on the way down.</p>
            </Col>
            <Col sm={4}>
                <ListGroup>
                    <ListGroup.Item>Muscle Group: Quads</ListGroup.Item>
                    <ListGroup.Item>Effort: High</ListGroup.Item>
                    <ListGroup.Item>Sets: 3</ListGroup.Item>
                    <ListGroup.Item>Reps: 8</ListGroup.Item>
                    <ListGroup.Item>Duration: 4 min</ListGroup.Item>
                </ListGroup>
            </Col>
        </Row>
        <Row>
            {/* consider changing styles */}
            <iframe width="560" height="315" src="https://www.youtube.com/embed/YUrXRgNjSmw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Row>
        <Row className="px-4 my-5">
            <h3>Related</h3>
        </Row>
        <Row className="px-4 my-5">
            <EquipmentCard1 />
            <EquipmentCard1 />
            <EquipmentCard1 />
        </Row>
        </Container>
        </Container>
        </main>
        </div>
    );
}