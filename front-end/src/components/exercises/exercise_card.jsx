import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Link } from 'react-router-dom';

function ExerciseCard(props) {
    return (
      <Card className='m-5' style={{ width: '20rem' }}>
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroup.Item><b>Category:</b> {props.category}</ListGroup.Item>
          <ListGroup.Item><b>Level:</b> {props.level}</ListGroup.Item>
          <ListGroup.Item><b>Force:</b> {props.force == "" ? props.force : "N/A"}</ListGroup.Item>
          <ListGroup.Item><b>Primary Muscles:</b> {props.primary_muscles}</ListGroup.Item>
          <ListGroup.Item><b>Equipment:</b> {props.equipment}</ListGroup.Item>
        </ListGroup>
        <Card.Body>
          <Link className='btn btn-primary' role='button' to={props.link}>See More</Link>
        </Card.Body>
      </Card>
    );
  }
  
  export default ExerciseCard;