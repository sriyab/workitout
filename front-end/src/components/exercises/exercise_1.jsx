import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import EquipmentCard1 from "../equipment/equipment_card_1";

export default function Exercise1() {
    return(
        <div>
    <main>
    <Container>
    <Container>
    <Row className="px-4 my-5">
        <Col sm={4}>
            <Image src="http://d205bpvrqc9yn1.cloudfront.net/0430.gif" fluid rounded />
        </Col>
        <Col sm={4}>
            <h1 class="font-weight-light">Dumbbell Triceps Extension</h1>
            <p class="mt-4">Position one dumbbell over head with both hands under inner plate (heart shaped grip). With elbows over head, lower forearm behind upper arm by flexing elbows. Flex wrists at bottom to avoid hitting dumbbell on back of neck. Raise dumbbell over head by extending elbows while hyperextending wrists. Return and repeat.</p>
        </Col>
        <Col sm={4}>
            <ListGroup>
                <ListGroup.Item>Muscle Group: Triceps</ListGroup.Item>
                <ListGroup.Item>Effort: Low</ListGroup.Item>
                <ListGroup.Item>Sets: 4</ListGroup.Item>
                <ListGroup.Item>Reps: 10</ListGroup.Item>
                <ListGroup.Item>Duration: 5 min</ListGroup.Item>
            </ListGroup>
        </Col>
    </Row>
    <Row>
        {/* consider changing styles */}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/nRiJVZDpdL0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </Row>
    <Row className="px-4 my-5">
        <h3>Related</h3>
    </Row>
    <Row className="px-4 my-5">
        <EquipmentCard1 />
        <EquipmentCard1 />
        <EquipmentCard1 />
    </Row>
    </Container>
    </Container>
    </main>
    </div>
    );
}