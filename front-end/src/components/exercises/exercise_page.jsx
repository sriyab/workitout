import {Container, Row, Col, Image} from "react-bootstrap";
import { ListGroup } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import ExerciseCard from "../exercises/exercise_card";
import EquipmentCard from "../equipment/equipment_card";
import RoutineCard from "../routines/routine_card";
import {getExercise, getEquipment, getRoutine, getExercisesRelated} from "../../api-calls/frontend_calls.jsx"

function RelatedEquipmentCards({ relatedEquipment }) {
return (
    <>
        {relatedEquipment?.map(curr => (
        <EquipmentCard
        name={curr.name}
        category={curr.category}
        price={"$" + curr.price_low + " - $" + curr.price_high}
        suitability={curr.home_suitable ? 'Yes' : 'No'}
        muscles={curr.muscles}
        link={'/equipment/'+ curr.id}
        />
        ))}
    </>
    );
}

function RelatedRoutineCards({ relatedRoutines }) {
return (
    <>
        {relatedRoutines?.map(curr => (
        <RoutineCard
        name={curr.name}
        author={curr.author}
        difficulty={curr.difficulty.toString()}
        length={curr.length.toString() + ' minutes'}
        type={curr.type}
        link={'/routines/'+ curr.id}
        />
        ))}
    </>
    );
}

export default function ExercisePage() {
    const id = useParams().exerciseID;
    const exercise = getExercise(id);
    const relatedIDs = getExercisesRelated(useParams());

    if(exercise == null) {
        return <h2>404: Page Not Found</h2>;
    }

    
    console.log(relatedIDs);
    var relatedEquipment = [];
    var relatedRoutines = [];

    for (let index in relatedIDs['equipment']) {
        const eqID = relatedIDs['equipment'][index];
        relatedEquipment.push(getEquipment(eqID));
    }

    for (let index in relatedIDs['routines']) {
        const rID = relatedIDs['routines'][index];
        relatedRoutines.push(getRoutine(rID));
    }

    return(
        <div>
        <main>
        <Container>
        <Container>
        <Row className="px-4 my-5">
            <Col sm={4}>
                <Image src={exercise.images[0]} fluid rounded />
            </Col>
            <Col sm={4}>
                <h1 class="font-weight-light">{exercise.name}</h1>
                <p class="mt-4">{JSON.parse(exercise.instructions.replace("{", "[").replace("}", "]")).join(" ")}</p>
            </Col>
            <Col sm={4}>
                <ListGroup>
                    <ListGroup.Item>Category: {exercise.category}</ListGroup.Item>
                    <ListGroup.Item>Level: {exercise.level}</ListGroup.Item>
                    <ListGroup.Item>Force: {exercise.force ? exercise.force : "N/A"}</ListGroup.Item>
                    <ListGroup.Item>Mechanic: {exercise.mechanic}</ListGroup.Item>
                    <ListGroup.Item>Primary Muscles: {exercise.muscles_primary}</ListGroup.Item>
                    <ListGroup.Item>Secondary Muscles: {exercise.muscles_secondary_string != "{}" ? exercise.muscles_secondary.join(", ") : "none"}</ListGroup.Item>
                    <ListGroup.Item>Equipment: {exercise.equipment_name}</ListGroup.Item>
                </ListGroup>
            </Col>
        </Row>
        <Row>
            {/* consider changing styles */}
            <iframe width="560" height="315" src={exercise.videos[0]} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </Row>
        <Row className="px-4 my-5">
            <h3>Related</h3>
        </Row>
        <Row className="px-4 my-5">
            {<RelatedEquipmentCards relatedEquipment={relatedEquipment}/>}
            {<RelatedRoutineCards relatedRoutines={relatedRoutines}/>}
        </Row>
        </Container>
        </Container>
        </main>
        </div>
        );
}
