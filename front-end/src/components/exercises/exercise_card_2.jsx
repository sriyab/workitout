import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import exercise_squat from '../../images/exercise_squat.jpg';
import { Link } from 'react-router-dom';

function ExerciseCard2() {
    return(
        <Card className='m-5' style={{ width: '20rem' }}>
        {/* <Card.Img variant="top" src={exercise_squat} /> */}
        <Card.Body>
          <Card.Title>Biceps Curl with Dumbbell</Card.Title>
          {/* <Card.Text>
            An exercise
          </Card.Text> */}
        </Card.Body>
        {/* for use in instance page: http://d205bpvrqc9yn1.cloudfront.net/0201.gif */}
        <ListGroup className="list-group-flush">
          <ListGroup.Item>Muscle Group: Biceps</ListGroup.Item>
          <ListGroup.Item>Effort: Medium</ListGroup.Item>
          <ListGroup.Item>Sets: 4</ListGroup.Item>
          <ListGroup.Item>Reps: 10</ListGroup.Item>
          <ListGroup.Item>Duration: 5 min</ListGroup.Item>
        </ListGroup>
        <Card.Body>
        <Link className='btn btn-primary' role='button' to='/exercises/ex2'>See More</Link>
        </Card.Body>
      </Card>
    );
}

export default ExerciseCard2;