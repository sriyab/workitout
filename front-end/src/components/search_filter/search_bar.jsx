import React from 'react';

function SearchBar(props) {
  const BarStyling = {width:"20rem",background:"#F2F1F9", border:"none", padding:"0.5rem"};
  return (
      <input 
      style={BarStyling}
      value={props.searchText}
      placeholder={"Search"}
      onChange={(e) => props.setSearchText(e.target.value)}
      />
    );
}

export default SearchBar