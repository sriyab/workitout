from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By

import unittest


url = "https://www.workit0ut.me/"

# referenced from https://nander.cc/using-selenium-within-a-docker-container

class GuiTests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome(options=chrome_options)
        
    def tearDown(self):
        self.driver.quit()

    # Test 1
    def testTitle(self):
        self.driver.get(url)
        self.assertEqual(self.driver.title, "WorkItOut")
    
    # Test 2
    def testExercises(self):
        self.driver.get(url + 'exercises')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Exercises")

    # Test 3
    def testEquipment(self):
        self.driver.get(url + 'equipment')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Equipment")

    # Test 4
    def testRoutines(self):
        self.driver.get(url + 'routines')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Workout Routines")

    # Test 5
    def testAbout(self):
        self.driver.get(url + 'about')
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About our Site")

    # Test 6
    def testNav(self):
        self.driver.get(url + 'about')

        self.assertEqual(
            self.driver.find_elements(By.CLASS_NAME, "nav-link")[0].get_attribute('href'),
            url
        )

    # Test 7
    def testNav2(self):
        self.driver.get(url + 'about')

        self.assertEqual(
            self.driver.find_elements(By.CLASS_NAME, "nav-link")[2].get_attribute('href'),
            url + 'equipment'
        )
    
    # Test 8
    def testExerciseInstance(self):
        self.driver.get(url + 'exercises')
        btn = self.driver.find_element(By.CLASS_NAME, "btn-primary")
        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)

    # Test 9
    def testEquipmentInstance(self):
        self.driver.get(url + 'equipment')
        btn = self.driver.find_element(By.CLASS_NAME, "btn-primary")
        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)

    # Test 10
    def testRoutineInstance(self):
        self.driver.get(url + 'routines')
        btn = self.driver.find_element(By.CLASS_NAME, "btn-primary")
        tgt = btn.get_attribute('href')
        self.driver.execute_script("arguments[0].click();", btn)
        self.assertEqual(self.driver.current_url, tgt)



if __name__ == "__main__":
    unittest.main()
