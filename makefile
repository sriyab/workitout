.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# Serve site in development mode
serve:
	npm --prefix front-end start

# Install dependencies 
install:
	npm --prefix front-end install

all:

clean:
	rm -f  *.tmp
	rm -rf __pycache__
