# WorkItOut


### Project Insights
**Project Name**: WorkItOut

**Group**: IDB Group 10-5

**Members**: 

Member          | UT EID  | GitLab ID
--------------- | ------- | ---------
Sriya Bandaru   | sb56296 | sriyab
Angela Geronimo | abg2482 | angelagmo
Vaidehi Phirke  | vnp379  | vaidehiphirke
Yash Sharma     | ys24448 | sharma.yash.1309
Goldin Vo       | gbv235  | goldinvo

**Git SHA**: 709acbe97f066118f01b778bd4e4eb1737f38057

**URL**: https://gitlab.com/sriyab/workitout

**Description**: 
A webpage that is dedicated to promoting healthier living through physical fitness!

**Website URL**: https://workit0ut.me

**GitLab URL**: https://gitlab.com/sriyab/workitout

**GitLab Pipelines**: https://gitlab.com/sriyab/workitout/-/pipelines


**Project Leaders**:

Phase I - Sriya Bandaru

Phase II - Goldin Vo

Phase III - Angela Geronimo

Phase IV - Yash Sharma

**Completion Time (hrs)**:

[ Phase I ]
Member   | Estimated | Actual
:------: | :-------: | :-----:
Sriya    | 10        | 20
Angela   | 10        | 15
Vaidehi  | 10        | 10
Yash     | 10        | 14
Goldin   | 10        | 15

[ Phase II ]
Member   | Estimated | Actual
:------: | :-------: | :-----:
Sriya    | 30        | 40
Angela   | 28        | 40
Vaidehi  | 20        | 36
Yash     | 25        | 39
Goldin   | 27        | 40

[ Phase III ]
Member   | Estimated | Actual
:------: | :-------: | :-----:
Sriya    | 17        | 20
Angela   | 18        | 20
Vaidehi  | 10        | 10
Yash     | 15        | 18
Goldin   | 15        | 20

[ Phase IV]
Member   | Estimated | Actual
:------: | :-------: | :-----:
Sriya    | 16        | 16
Angela   | 20        | 15
Vaidehi  | 10        | 9
Yash     | 10        | 11
Goldin   | 17        | 15

_________________________________________________________________________

### Project Details

**Data Sources**:

https://www.tredict.com/glossary/

https://www.programmableweb.com/api/exercisedb-rest-api-v10

https://wger.de/en/software/api

https://open.cdc.gov/apis.html

https://www.healthgraphic.com/discovery

All data sources used as described in their API documentation.

**Models**:
- Exercises
- Equipment
- Routines

**Instances**:
- 150 Exercise instances
- 100 Equipment instances
- 50 Routine instances

**Attributes**:
- Exercises
    - Filterable/Sortable: name, force, level, mechanic, category
    - Other: images, instructions, muscles primary/secondary, equipment name, videos
- Equipment
    - Filterable/Sortable: name, category, muscles, price, suitability
    - Other: description, images, tips, videos
- Routines
    - Filterable/Sortable: name, author, difficulty, length, type
    - Other: overview, equipment, exercises, link

**Media**:
- Exercises: videos, images, text
- Equipment: videos, images, text
- Routines: images, links, text


**Questions**:
- How does different groups of exercises differentiate from each other?
- What injuries and illnesses can certain exercises prevent?
- What is the best equipment to use for exercising certain muscles?

